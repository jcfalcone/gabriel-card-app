<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\FilePlayersUpload;
use App\Http\Controllers\Players;
use App\Http\Controllers\CardExporter;
use App\Http\Controllers\ImportScore;
use App\Http\Controllers\CreateDashboard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


//Route::get('/upload-data', [FilePlayersUpload::class, 'createForm']);
Route::post('/upload-data', [FilePlayersUpload::class, 'fileUpload'])->name('fileUpload');
Route::post('/upload-score-data', [ImportScore::class, 'fileUpload'])->name('fileUpload');

Route::post('/players', [Players::class, 'searchPlayers'])->name('searchPlayers');
Route::post('/cardExporter', [CardExporter::class, 'export'])->name('export');
Route::post('/cardExporter', [CardExporter::class, 'export'])->name('export');

Route::post('/createDashboard', [CreateDashboard::class, 'export'])->name('export');

Route::get('/assets/{file}', function($filename)
{
    $files = Storage::files('players');  // set repo name of storage folder

    $path = storage_path('app/public/players/' . $filename);

    if (!File::exists($path)) {
        abort(404);
    }

    $file = File::get($path);
    $type = File::mimeType($path);

    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

    return $response;
});