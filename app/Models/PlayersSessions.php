<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PlayersSessions extends Model
{
    use HasFactory;
    protected $fillable = [
        'playerId',
        'sessionId'
    ];
}
