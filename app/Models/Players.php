<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Players extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'document',
        'email',
        'dorsal',
        'age',
        'weight',
        'height',
        'type'
    ];
    
    public function Agility($PlayerID, $Session, $Age)
    {
        $Total = DB::table('agilities')->where('playerId', '=', $PlayerID)
                                        ->where('sessionId', '=', $Session)
                                        ->sum('try');

        if(empty($Total) && $Total !== 0)
        {
            return 0;
        }

        $Score = DB::table('score_data')->where('type', '=', '5')
                                       ->where('gender', '=', 'M')
                                       ->where('age', '=', $Age)
                                       ->where('score', '<=', floor($Total))
                                       ->orderBy('score', 'DESC')
                                       ->orderBy('age')
                                       ->take(1)
                                       ->get(array( 'result' ))->first();

        return isset($Score->result) ? $Score->result : 0;
    }
    
    public function AgilityTry($PlayerID, $Session, $Sequence)
    {
        $Total = DB::table('agilities')->where('playerId', '=', $PlayerID)
                                        ->where('sessionId', '=', $Session)
                                        ->where('sequence', '=', $Sequence)
                                        ->sum('try');

        return $Total;
    }


    public function HighJump($PlayerID, $Session, $Age)
    {
        $Result = 0;
        $Total = 0;

        $Total = DB::table('vertical_jumps')->where('playerId', '=', $PlayerID)
                                                ->where('sessionId', '=', $Session)
                                                ->sum('try');

        $Score = DB::table('vertical_jumps')->where('playerId', '=', $PlayerID)
                                            ->where('sessionId', '=', $Session)
                                            ->orderBy('try', 'DESC')
                                            ->take(2)
                                            ->get(array('try'))->toArray();

        for($count = 0; $count < count($Score); $count++)
        {
            $Total += $Score[$count]->try;
        }

        if(empty($Total) && $Total !== 0)
        {
            return 0;
        }

        $Score = DB::table('score_data')->where('type', '=', '11')
                                       ->where('gender', '=', 'M')
                                       ->where('age', '=', $Age)
                                       ->where('score', '<=', floor($Total))
                                       ->orderBy('score', 'DESC')
                                       ->orderBy('age')
                                       ->take(1)
                                       ->get(array( 'result' ))->first();

        return isset($Score->result) ? $Score->result : 0;
    }

    public function Acceleration($PlayerID, $Session, $Age)
    {
        $KmBestSpeed = 0;

        $Speed = DB::table('accelerations')->where('playerId', '=', $PlayerID)
                                            ->where('sessionId', '=', $Session)
                                            ->orderBy('time')
                                            ->get(array('time'))->first();

        if($Speed && $Speed->time > 0)
        {
            $KmBestSpeed = (3600 * 10 / $Speed->time ) / 1000;
        }

        if(empty($KmBestSpeed) && $KmBestSpeed !== 0)
        {
            return 0;
        }

        $Score = DB::table('score_data')->where('type', '=', '3')
                                       ->where('gender', '=', 'M')
                                       ->where('age', '=', $Age)
                                       ->where('score', '<=', ($KmBestSpeed))
                                       ->orderBy('score', 'DESC')
                                       ->orderBy('age')
                                       ->take(1)
                                       ->get(array( 'result' ))->first();

        return isset($Score->result) ? $Score->result : 0;
    }

    public function AccuracyTry($PlayerID, $Session, $Sequence)
    {
        $KmBestSpeed = 0;

        $Speed = DB::table('shootings')->where('playerId', '=', $PlayerID)
                                            ->where('sessionId', '=', $Session)
                                            ->where('sequence', '=', $Sequence)
                                            ->get(array('pwr'))->first();

        if($Speed)
        {
            return $Speed->pwr;
        }

        return 0;
    }
    
    public function AccelerationKM($PlayerID, $Session)
    {
        $KmBestSpeed = 0;

        $Speed = DB::table('accelerations')->where('playerId', '=', $PlayerID)
                                            ->where('sessionId', '=', $Session)
                                            ->orderBy('time')
                                            ->get(array('time'))->first();

        if($Speed && $Speed->time > 0)
        {
            $KmBestSpeed = (3600 * 10 / $Speed->time ) / 1000;
        }

        return $KmBestSpeed;
    }

    public function Shotting($PlayerID, $Session, $Age)
    {
        $CountRow = DB::table('shootings')->where('playerId', '=', $PlayerID)
                                        ->where('sessionId', '=', $Session)
                                        ->where('score', '>', 0)
                                        ->count();

        if($CountRow == 0)
        {
            return 0;
        }

        $Result = 0;
        $Total = 0;

        $Score = DB::table('shootings')->where('playerId', '=', $PlayerID)
                                    ->where('sessionId', '=', $Session)
                                    ->where('score', '>', 0)
                                    ->orderBy('score', 'DESC')
                                    ->get(array('score'))->toArray();
                  
        for($count = 0; $count < count($Score); $count++)
        {
            if($count == 0)
            {
                $Total += $Score[$count]->score;

                if($CountRow > 2)
                {
                    $Total += $Score[$count]->score;
                }
            }
            else
            {
                $Total += $Score[$count]->score;
                break;
            }
        }

        $Total /= $CountRow;

        if(empty($Total) && $Total !== 0)
        {
            return 0;
        }

        


        $Score = DB::table('score_data')->where('type', '=', '7')
                                       ->where('gender', '=', 'M')
                                       ->where('age', '=', $Age)
                                       ->where('score', '<=', floor($Total))
                                       ->orderBy('score', 'DESC')
                                       ->orderBy('age')
                                       ->take(1)
                                       ->get(array( 'result' ))->first();

        return isset($Score->result) ? $Score->result : 0;
    }

    public function ShottingAccuracy($PlayerID, $Session, $Age)
    {
        $CountRow = DB::table('shootings')->where('playerId', '=', $PlayerID)
                                        ->where('sessionId', '=', $Session)
                                        ->count();

        if($CountRow == 0)
        {
            return 0;
        }

        $Result = 0;
        $Total = 0;
        $BestScore = 0;

        $Score = DB::table('shootings')->where('playerId', '=', $PlayerID)
                                    ->where('sessionId', '=', $Session)
                                    ->orderBy('score', 'DESC')
                                    ->get(array('score'))->toArray();

        for($count = 0; $count < count($Score); $count++)
        {
            if($count <= 1)
            {
                $BestScore += $Score[$count]->score;
            }

            $Total += $Score[$count]->score;
        }

        $FinalScore = $Total + $BestScore;
        
        if($CountRow > 0)
        {
            $FinalScore += ($Total / $CountRow);
        }

        if(empty($FinalScore) && $FinalScore !== 0)
        {
            return 0;
        }

        $Score = DB::table('score_data')->where('type', '=', '8')
                                       ->where('gender', '=', 'M')
                                       ->where('age', '=', $Age)
                                       ->where('score', '<=', floor($FinalScore))
                                       ->orderBy('score', 'DESC')
                                       ->orderBy('age')
                                       ->take(1)
                                       ->get(array( 'result' ))->first();

        return isset($Score->result) ? $Score->result : 0;
    }

    public function ShottingPower($PlayerID, $Session, $Age)
    {

        $Score = DB::table('shootings')->where('playerId', '=', $PlayerID)
                                    ->where('sessionId', '=', $Session)
                                    ->orderBy('score', 'DESC')
                                    ->max('score');

        if(empty($Score) && $Score !== 0)
        {
            return 0;
        }

        $Score = DB::table('score_data')->where('type', '=', '9')
                                       ->where('gender', '=', 'M')
                                       ->where('age', '=', $Age)
                                       ->where('score', '<=', floor($Score))
                                       ->orderBy('score', 'DESC')
                                       ->orderBy('age')
                                       ->take(1)
                                       ->get(array( 'result' ))->first();

        return isset($Score->result) ? $Score->result : 0;
    }

    public function ShottingData($PlayerID, $Session)
    {
        $ShotAccuracy = 0;
        $ShotPower = 0;

        $MaxPower = DB::table('shootings')  ->where('playerId', '=', $PlayerID)
                                            ->where('sessionId', '=', $Session)
                                            ->max('pwr');

        if($MaxPower)
        {
            $ShotAccuracy = $MaxPower;
        }

        $MaxPowerTotal = DB::table('shootings')  ->where('playerId', '=', $PlayerID)
                                                 ->where('sessionId', '=', $Session)
                                                 ->sum('precition');


        $CountRow = DB::table('shootings')  ->where('playerId', '=', $PlayerID)
                                            ->where('sessionId', '=', $Session)
                                            ->count();

        $Score = DB::table('shootings') ->where('playerId', '=', $PlayerID)
                                        ->where('sessionId', '=', $Session)
                                        ->take(2)
                                        ->orderBy('precition', 'DESC')
                                        ->get(array('precition'))->toArray();
                                        
        $BestTime = 0;
        $Average = 0;

        for($count = 0; $count < count($Score); $count++)
        {
            $BestTime += $Score[$count]->precition;
        }

        if($CountRow > 0)
        {
            $Average = ($MaxPowerTotal / $CountRow);
        }

        $ShotPower = $MaxPowerTotal + $Average + $BestTime;

        $Data = array();

        $Data['ShotAccuracy']   = $ShotAccuracy;
        $Data['ShotPower']      = $ShotPower;

        $Position = DB::table('shootings') ->where('playerId', '=', $PlayerID)
                                        ->where('sessionId', '=', $Session)
                                        ->orderBy('sequence')
                                        ->get(array('column', 'line'))->toArray();

        $Data['ShotPosition'] = $Position;

        return $Data;
    }

    public function Dribble($PlayerID, $Session, $Age)
    {
        $Total = DB::table('dribbles')->where('playerId', '=', $PlayerID)
                                        ->where('sessionId', '=', $Session)
                                        ->sum('try');

        if(empty($Total) && $Total !== 0)
        {
            return 0;
        }

        $Score = DB::table('score_data')->where('type', '=', '6')
                                       ->where('gender', '=', 'M')
                                       ->where('age', '=', $Age)
                                       ->where('score', '<=', floor($Total))
                                       ->orderBy('score', 'DESC')
                                       ->orderBy('age')
                                       ->take(1)
                                       ->get(array( 'result' ))->first();


        return isset($Score->result) ? $Score->result : 0;
    }

    public function DribbleTry($PlayerID, $Session, $Sequence)
    {
        $Total = DB::table('dribbles')->where('playerId', '=', $PlayerID)
                                        ->where('sessionId', '=', $Session)
                                        ->where('sequence', '=', $Sequence)
                                        ->sum('try');


        return $Total;
    }

    public function Speed($PlayerID, $Session, $Age)
    {
        $KmBestSpeed = 0;

        $Speed = DB::table('speeds')->where('playerId', '=', $PlayerID)
                                    ->where('sessionId', '=', $Session)
                                    ->orderBy('time')
                                    ->get(array('time'))->first();

        if($Speed)
        {
            $KmBestSpeed = (3600 * 40 / $Speed->time ) / 1000; 
        }
        else
        {
            return 0;
        }

        $KmBestSpeedPerc = $KmBestSpeed * 0.85;

        $BestTime = 0;
        $WostTime = 0;

        $Stamina = DB::table('staminas')->where('playerId', '=', $PlayerID)
                                        ->where('sessionId', '=', $Session)
                                        ->take(3)
                                        ->orderBy('time')
                                        ->get(array('time'))->toArray();

        for($count = 0; $count < count($Stamina); $count++)
        {
            $BestTime += $Stamina[$count]->time;
        }

        if($BestTime > 0)
        {
            $BestTime = ((3600 * 30 / ($BestTime / 3)) / 1000) * 0.1;
        }

        $Stamina = DB::table('staminas')->where('playerId', '=', $PlayerID)
                                        ->where('sessionId', '=', $Session)
                                        ->take(3)
                                        ->orderBy('time', 'DESC')
                                        ->get(array('time'))->toArray();

        for($count = 0; $count < count($Stamina); $count++)
        {
            $WostTime += $Stamina[$count]->time;
        }

        if($WostTime > 0)
        {
            $WostTime = ((3600 * 30 / ($WostTime / 3)) / 1000) * 0.05;
        }

        $KmBestSpeedPerc += $BestTime + $WostTime;

        if(empty($KmBestSpeedPerc) && $KmBestSpeedPerc !== 0)
        {
            return 0;
        }

        $Score = DB::table('score_data')->where('type', '=', '4')
                                       ->where('gender', '=', 'M')
                                       ->where('age', '=', $Age)
                                       ->where('score', '<=', ($KmBestSpeedPerc))
                                       ->orderBy('score', 'DESC')
                                       ->orderBy('age')
                                       ->take(1)
                                       ->get(array( 'result' ))->first();
                                
        return isset($Score->result) ? $Score->result : 0;
    }

    public function SpeedKM($PlayerID, $Session)
    {
        $KmBestSpeed = 0;

        $Speed = DB::table('speeds')->where('playerId', '=', $PlayerID)
                                    ->where('sessionId', '=', $Session)
                                    ->orderBy('time')
                                    ->get(array('time'))->first();

        if($Speed)
        {
            $KmBestSpeed = (3600 * 40 / $Speed->time ) / 1000; 
        }
        else
        {
            return 0;
        }

        return $KmBestSpeed;
    }

    public function LongPass($PlayerID, $Session, $Age)
    {
        $Pass = DB::table('long_passes')->where('playerId', '=', $PlayerID)
                                    ->where('sessionId', '=', $Session)
                                    ->get(array('pass'))->toArray();
        
        $PassList = array();                        

        for($count = 0; $count < count($Pass); $count++)
        {
            switch($Pass[$count]->pass)
            {
                case -2:
                    $PassList[] = 88;
                    break;
                case -3:
                    $PassList[] = 91;
                    break;
                case -4:
                    $PassList[] = 94;
                    break;
                case -5:
                    $PassList[] = 97;
                    break;
                case -6:
                    $PassList[] = 100;
                    break;
                case -1:
                    $PassList[] = 85;
                    break;
                case -10:
                    $PassList[] = 70;
                    break;
                case -11:
                    $PassList[] = 75;
                    break;
                case 5:
                    $PassList[] = 45;
                    break;
                case 10:
                    $PassList[] = 50;
                    break;
                case 15:
                    $PassList[] = 55;
                    break;
                case 20:
                    $PassList[] = 60;
                    break;
                case 25:
                    $PassList[] = 65;
                    break;
            }
        }

        arsort($PassList);

        $PassList = array_values($PassList);

        $Count = count($PassList);
        $Total = 0;

        if($Count >= 7)
        {
            $PassList = array_splice($PassList, 0, $Count - 2);

            $NewCount = count($PassList);
            $Total = array_sum($PassList) / $NewCount;

            if($Count >= 9)
            {
                $Total += 2;
            }

            if($Count >= 11)
            {
                $Total += 1;
            }
        }
        else if($Count >= 6)
        {
            $PassList = array_splice($PassList, 1, $Count - 1);

            $NewCount = count($PassList);
            $Total = array_sum($PassList) / $NewCount;
        }
        else if($Count >= 5)
        {
            $Total = array_sum($PassList) / $Count;
        }

        if(empty($Total) && $Total !== 0)
        {
            return 0;
        }

        $Score = DB::table('score_data')->where('type', '=', '1')
                                       ->where('gender', '=', 'M')
                                       ->where('age', '=', $Age)
                                       ->where('score', '<=', floor($Total))
                                       ->orderBy('score', 'DESC')
                                       ->orderBy('age')
                                       ->take(1)
                                       ->get(array( 'result' ))->first();

        return isset($Score->result) ? $Score->result : 0;
    }

    public function LongPassPasses($PlayerID, $Session)
    {
        $Pass = DB::table('long_passes')->where('playerId', '=', $PlayerID)
                                    ->where('sessionId', '=', $Session)
                                    ->get(array('pass', 'sequence'))->toArray();

        return $Pass;
    }

    public function LongPassHand($PlayerID, $Session, $Age)
    {
        $Pass = DB::table('long_pass_hands')->where('playerId', '=', $PlayerID)
                                            ->where('sessionId', '=', $Session)
                                            ->get(array('pass'))->toArray();
        
        $PassList = array();                        

        for($count = 0; $count < count($Pass); $count++)
        {
            switch($Pass[$count]->pass)
            {
                case -2:
                    $PassList[] = 88;
                    break;
                case -3:
                    $PassList[] = 91;
                    break;
                case -4:
                    $PassList[] = 94;
                    break;
                case -5:
                    $PassList[] = 97;
                    break;
                case -6:
                    $PassList[] = 100;
                    break;
                case -1:
                    $PassList[] = 85;
                    break;
                case -10:
                    $PassList[] = 75;
                    break;
                case -11:
                    $PassList[] = 78;
                    break;
                case 5:
                    $PassList[] = 45;
                    break;
                case 10:
                    $PassList[] = 50;
                    break;
                case 15:
                    $PassList[] = 55;
                    break;
                case 20:
                    $PassList[] = 60;
                    break;
                case 25:
                    $PassList[] = 75;
                    break;
            }
        }

        arsort($PassList);

        $PassList = array_values($PassList);

        $Count = count($PassList);
        $Total = 0;

        if($Count >= 7)
        {
            $PassList = array_splice($PassList, 0, $Count - 2);

            $NewCount = count($PassList);
            $Total = array_sum($PassList) / $NewCount;

            if($Count >= 9)
            {
                $Total += 2;
            }

            if($Count >= 11)
            {
                $Total += 1;
            }
        }
        else if($Count >= 6)
        {
            $PassList = array_splice($PassList, 1, $Count - 1);

            $NewCount = count($PassList);
            $Total = array_sum($PassList) / $NewCount;
        }
        else if($Count >= 5)
        {
            $Total = array_sum($PassList) / $Count;
        }

        if(empty($Total) && $Total !== 0)
        {
            return 0;
        }

        $Score = DB::table('score_data')->where('type', '=', '2')
                                       ->where('gender', '=', 'M')
                                       ->where('age', '=', $Age)
                                       ->where('score', '<=', floor($Total))
                                       ->orderBy('score', 'DESC')
                                       ->orderBy('age')
                                       ->take(1)
                                       ->get(array( 'result' ))->first();

        return isset($Score->result) ? $Score->result : 0;
    }

    public function LongPassHandPasses($PlayerID, $Session)
    {
        $Pass = DB::table('long_pass_hands')->where('playerId', '=', $PlayerID)
                                            ->where('sessionId', '=', $Session)
                                            ->where('pass', '>=', 0)
                                            ->get(array('pass'))->toArray();

        return $Pass;
    }

    public function Stamina($PlayerID, $Session, $Age)
    {
        $Score = DB::table('staminas')->where('playerId', '=', $PlayerID)
                                    ->where('sessionId', '=', $Session)
                                    ->orderBy('time')
                                    ->take(3)
                                    ->get(array('time'))->toArray();

        $BestTime = 0;

        for($count = 0; $count < count($Score); $count++)
        {
            $BestTime += $Score[$count]->time;
        }

        $Score = DB::table('staminas')->where('playerId', '=', $PlayerID)
                                    ->where('sessionId', '=', $Session)
                                    ->orderBy('time', 'DESC')
                                    ->take(3)
                                    ->get(array('time'))->toArray();

        $WhostTime = 0;

        for($count = 0; $count < count($Score); $count++)
        {
            $WhostTime += $Score[$count]->time;
        }

        $Fatigue = 0;
        $FatigueScoreStart = -1;
        $FatigueScoreEnd = -1;
        $KmBest = 0;

        $AvgBest = ($BestTime / 3);
        $AvgWost = ($WhostTime / 3);

        if($WhostTime != 0)
        {
            $Fatigue = round(($AvgBest / $AvgWost)  * 100);

            if($Fatigue < 80)
            {
                $FatigueScoreStart = 55;
                $FatigueScoreEnd = 57;
            }
            elseif($Fatigue < 81)
            {
                $FatigueScoreStart = 58;
                $FatigueScoreEnd = 59;
            }
            elseif($Fatigue < 82)
            {
                $FatigueScoreStart = 60;
                $FatigueScoreEnd = 62;
            }
            elseif($Fatigue < 85)
            {
                $FatigueScoreStart = 63;
                $FatigueScoreEnd = 68;
            }
            elseif($Fatigue < 88)
            {
                $FatigueScoreStart = 69;
                $FatigueScoreEnd = 75;
            }
            elseif($Fatigue < 90)
            {
                $FatigueScoreStart = 76;
                $FatigueScoreEnd = 79;
            }
            elseif($Fatigue < 96)
            {
                $FatigueScoreStart = 80;
                $FatigueScoreEnd = 86;
            }
            else
            {
                $FatigueScoreStart = 87;
                $FatigueScoreEnd = 999;
            }
        }

        if($BestTime != 0)
        {
            $KmBest = ((3600 * 30) / $AvgBest) / 1000;
        }

        if(empty($FatigueScoreStart) && $FatigueScoreStart !== 0)
        {
            return 0;
        }

        if(empty($FatigueScoreEnd) && $FatigueScoreEnd !== 0)
        {
            return 0;
        }

        $Score = DB::table('score_data')->where('type', '=', '10')
                                       ->where('gender', '=', 'M')
                                       ->where('age', '=', $Age)
                                       ->where('score', '<=', floor($KmBest))

                                       ->where('result', '>=', $FatigueScoreStart)
                                       ->where('result', '<=', $FatigueScoreEnd)

                                       ->orderBy('score', 'DESC')
                                       ->orderBy('age')
                                       ->take(1)
                                       ->get(array( 'result' ))->first();

        if(!isset($Score->result))
        {
            $Score = DB::table('score_data')->where('type', '=', '10')
                                           ->where('gender', '=', 'M')
                                           ->where('age', '=', $Age)
                                           ->where('score', '>', floor($KmBest))
    
                                           ->where('result', '>=', $FatigueScoreStart)
    
                                           ->orderBy('score')
                                           ->orderBy('age')
                                           ->take(1)
                                           ->get(array( 'result' ))->first();
        }

        return isset($Score->result) ? $Score->result : 0;
    }

    public function StaminaData($PlayerID, $Session)
    {
        $Score = DB::table('staminas')->where('playerId', '=', $PlayerID)
                                    ->where('sessionId', '=', $Session)
                                    ->orderBy('time')
                                    ->take(3)
                                    ->get(array('time'))->toArray();

        $BestTime = 0;

        for($count = 0; $count < count($Score); $count++)
        {
            $BestTime += $Score[$count]->time;
        }

        $Score = DB::table('staminas')->where('playerId', '=', $PlayerID)
                                    ->where('sessionId', '=', $Session)
                                    ->orderBy('time', 'DESC')
                                    ->take(3)
                                    ->get(array('time'))->toArray();

        $WhostTime = 0;

        for($count = 0; $count < count($Score); $count++)
        {
            $WhostTime += $Score[$count]->time;
        }

        $Fatigue = 0;

        if($WhostTime != 0)
        {
            $Fatigue = (($BestTime / 3) / ($WhostTime / 3) * 100);
        }

        $ReturnData = array();

        $BestTimeMs = floor(($BestTime * 100) - (floor($BestTime) * 100));
        $WhostTimeMs = floor(($WhostTime * 100) - (floor($WhostTime) * 100));

        $ReturnData['BestTime']  = floor($BestTime).'s '.$BestTimeMs.'ms';
        $ReturnData['WorstTime'] = floor($WhostTime).'s '.$WhostTimeMs.'ms';
        $ReturnData['Fatigue']   = floor($Fatigue);
        $ReturnData['Battery']   = 1;

        if($Fatigue >= 97)
        {
            $ReturnData['Battery']   = 6;
        }
        elseif($Fatigue > 90)
        {
            $ReturnData['Battery']   = 5;
        }
        elseif($Fatigue > 80)
        {
            $ReturnData['Battery']   = 4;
        }
        elseif($Fatigue > 70)
        {
            $ReturnData['Battery']   = 3;
        }
        elseif($Fatigue > 60)
        {
            $ReturnData['Battery']   = 2;
        }

        return $ReturnData;
    }

    public function ShortPass($PlayerID, $Session, $Age)
    {
        $query =  DB::table('short_passes') ->join('touches', function($join)
                                            {
                                                $join->on('touches.playerId', '=', 'short_passes.playerId');
                                                $join->on('touches.sessionId', '=', 'short_passes.sessionId');
                                                $join->on('touches.sequence', '=', 'short_passes.sequence');
                                            })
                                            ->where('short_passes.playerId', '=', $PlayerID)
                                            ->where('short_passes.sessionId', '=', $Session)
                                            ->orderBy('short_passes.sequence')
                                            ->get(array('touches.left',
                                                        'touches.right',
                                                        'short_passes.try'))->toArray();

        $TotalLeft = 0;
        $TotalRight = 0;
        $TotalTarget = 0;
        $Ratio = 0;
        $Perc = 0;

        for($count = 0; $count < count($query); $count++)
        {
            $TotalLeft += $query[$count]->left;
            $TotalRight += $query[$count]->right;
            $TotalTarget += $query[$count]->try;
        }

        if($TotalRight > 0)
        {
            if($TotalRight > $TotalLeft)
            {
                $Perc = ($TotalLeft / $TotalRight) * 100;
            }
            else
            {
                $Perc = ($TotalRight / $TotalLeft) * 100;
            }
        }

        if($TotalRight > 0 && $TotalTarget > 0)
        {
            $Ratio = ($TotalLeft + $TotalRight) / $TotalTarget;
        }

        if($Perc > 60 && $Ratio < 1.65)
        {
            $TotalTarget += 2;
        }

        if(empty($TotalTarget) && $TotalTarget !== 0)
        {
            return 0;
        }

        $Score = DB::table('score_data')->where('type', '=', '0')
                                       ->where('gender', '=', 'M')
                                       ->where('age', '=', $Age)
                                       ->where('score', '<=', floor($TotalTarget))
                                       ->orderBy('score', 'DESC')
                                       ->orderBy('age')
                                       ->take(1)
                                       ->get(array( 'result' ))->first();

        return isset($Score->result) ? $Score->result : 0;
    }

    public function ShortPassTry($PlayerID, $Session, $Sequence)
    {
        $query =  DB::table('short_passes') ->join('touches', function($join)
                                            {
                                                $join->on('touches.playerId', '=', 'short_passes.playerId');
                                                $join->on('touches.sessionId', '=', 'short_passes.sessionId');
                                                $join->on('touches.sequence', '=', 'short_passes.sequence');
                                            })
                                            ->where('short_passes.playerId', '=', $PlayerID)
                                            ->where('short_passes.sessionId', '=', $Session)
                                            ->where('touches.sequence', '=', $Sequence)
                                            ->where('short_passes.sequence', '=', $Sequence)
                                            ->orderBy('short_passes.sequence')
                                            ->get(array('touches.left',
                                                        'touches.right',
                                                        'short_passes.try'))->toArray();


        if(count($query) == 0)
        {
            $Return = array();
    
            $Return['Ratio']    = 0;
            $Return['Perc']     = 0;
            $Return['Target']   = 0;
    
            return $Return;
        }

        $TotalLeft = 0;
        $TotalRight = 0;
        $TotalTarget = 0;
        $Ratio = 0;
        $Perc = 0;

        for($count = 0; $count < count($query); $count++)
        {
            $TotalLeft += $query[$count]->left;
            $TotalRight += $query[$count]->right;
            $TotalTarget += $query[$count]->try;
        }

        if($TotalRight > 0)
        {
            if($TotalRight > $TotalLeft)
            {
                $Perc = ($TotalLeft / $TotalRight) * 100;
            }
            else
            {
                $Perc = ($TotalRight / $TotalLeft) * 100;
            }
        }

        if($TotalRight > 0 && $TotalTarget > 0)
        {
            $Ratio = ($TotalLeft + $TotalRight) / $TotalTarget;
        }

        if($Perc > 60)
        {
            $TotalTarget += 1;
        }

        if($Ratio < 1.65)
        {
            $TotalTarget += 1;
        }

        $Return = array();

        $Return['Ratio']    = $Ratio;
        $Return['Perc']     = $Perc;
        $Return['Target']   = $TotalTarget;

        return $Return;
    }
    
    public function VerticalJump($PlayerID, $Session, $Age)
    {
        $Jump = DB::table('vertical_jumps')->where('playerId', '=', $PlayerID)
                                            ->where('sessionId', '=', $Session)
                                            ->take(3)
                                            ->get(array('try'))->toArray();

        $Total = 0;

        for($count = 0; $count < count($Jump); $count++)
        {
            if($count < 2)
            {
                $Total += $Jump[$count]->try;
            }

            $Total += $Jump[$count]->try;
        }

        $Score = DB::table('score_data')->where('type', '=', '11')
                                       ->where('gender', '=', 'M')
                                       ->where('age', '=', $Age)
                                       ->where('score', '<=', floor($Total))
                                       ->orderBy('score', 'DESC')
                                       ->orderBy('age')
                                       ->take(1)
                                       ->get(array( 'result' ))->first();

        return isset($Score->result) ? $Score->result : 0;
    }

    public function VerticalJumpData($PlayerID, $Session)
    {
        $Jump = DB::table('vertical_jumps')->where('playerId', '=', $PlayerID)
                                            ->where('sessionId', '=', $Session)
                                            ->take(3)
                                            ->orderBy('try')
                                            ->get(array('try', 'sequence'))->toArray();


        $ReturnData = array();

        for($count = 0; $count < count($Jump); $count++)
        {
            $ReturnData[$Jump[$count]->sequence] =  new \stdClass();
            $ReturnData[$Jump[$count]->sequence]->try = $Jump[$count]->try;

            switch($Jump[$count]->sequence)
            {
                case 0:
                    $ReturnData[$Jump[$count]->sequence]->Text = '1st';
                    break;
                case 1:
                    $ReturnData[$Jump[$count]->sequence]->Text = '2nd';
                    break;
                case 2:
                    $ReturnData[$Jump[$count]->sequence]->Text = '3rd';
                    break;
            }

            switch($count)
            {
                case count($Jump) - 1:
                    $ReturnData[$Jump[$count]->sequence]->Line = 'Attempts/Attempt'.($Jump[$count]->sequence + 1).'/Line_Up.png';
                    $ReturnData[$Jump[$count]->sequence]->Width = 203;
                    $ReturnData[$Jump[$count]->sequence]->Height = 75;

                    $ReturnData[$Jump[$count]->sequence]->Offset = 63;
                    $ReturnData[$Jump[$count]->sequence]->TextOffset = 90;
                    $ReturnData[$Jump[$count]->sequence]->TextAttemptOffset = 85;
                    break;
                case 0:
                    $ReturnData[$Jump[$count]->sequence]->Line = 'Attempts/Attempt'.($Jump[$count]->sequence + 1).'/Line_Down.png';
                    $ReturnData[$Jump[$count]->sequence]->Width = 203;
                    $ReturnData[$Jump[$count]->sequence]->Height = 75;

                    $ReturnData[$Jump[$count]->sequence]->Offset = 15;
                    $ReturnData[$Jump[$count]->sequence]->TextOffset = 0;
                    $ReturnData[$Jump[$count]->sequence]->TextAttemptOffset = -10;
                    break;
                default:
                    $ReturnData[$Jump[$count]->sequence]->Line = 'Attempts/Attempt'.($Jump[$count]->sequence + 1).'/Line.png';
                    $ReturnData[$Jump[$count]->sequence]->Width = 203;
                    $ReturnData[$Jump[$count]->sequence]->Height = 75;

                    $ReturnData[$Jump[$count]->sequence]->Offset = 38;
                    $ReturnData[$Jump[$count]->sequence]->TextOffset = 48;
                    $ReturnData[$Jump[$count]->sequence]->TextAttemptOffset = 40;
                    break;
            }
        }

        return $ReturnData;
    }

    public function AgeGroup($Age)
    {
        $YearSeason = date("Y");

        if(date('m') < 8)
        {
            $YearSeason = date("Y",strtotime("-1 year"));
        }

        $YearAge = date("Y",strtotime("-".$Age." year"));

        $Result = ($YearSeason - $YearAge) + 1;

        switch($Result)
        {
            case 5:
                return 'Pré-Petizes';
            case 6:
            case 7:
                return 'Petizes';
            case 8:
            case 9:
                return 'Traquinas';
            case 10:
            case 11:
                return 'Benjamins';
            case 12:
            case 13:
                return 'Infantis';
            case 14:
            case 15:
                return 'Iniciados';
            case 16:
            case 17:
                return 'Juvenis';
            case 18:
            case 19:
                return 'Juniores';
        }

        return "Unknown";
    }
}
