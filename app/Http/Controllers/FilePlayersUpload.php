<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\File_Players;
use App\Models\TrainingSession;
use App\Models\Players;
use App\Models\PlayersSessions;
use App\Models\LongPass;
use App\Models\LongPassHand;
use App\Models\Touches;
use App\Models\Agility;
use App\Models\Dribble;
use App\Models\Shooting;
use App\Models\Speed;
use App\Models\Acceleration;
use App\Models\Stamina;
use App\Models\ShortPass;
use App\Models\BiometricData;
use App\Models\VerticalJump;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

class FilePlayersUpload extends Controller
{
    private $Error;

    public function createForm()
    {
        return view('file-upload');
    }

    public function fileUpload(Request $req)
    {
        //private $sessionId = -1;
        //private $fileID = -1;
    
        /*$req->validate([
            'file' => 'required|mimes:xlsx|max:2048'
        ]);*/

        if(!$req->file()) {
            return response()->json([
                "code"    => 400,
                "message" => "Please select a file!"
            ], 200);
        }

        $fileModel = new File_Players;
        
        if($req->file()) {
            $fileName = time().'_'.$req->file->getClientOriginalName();
            $filePath = $req->file('file')->storeAs('uploads', $fileName, 'public');
            $fileModel->file = time().'_'.$req->file->getClientOriginalName();
            $fileModel->path = '/storage/' . $filePath;
            $fileModel->type = 1;
            $fileModel->save();

            $this->fileID = $fileModel->id;

            $reader = IOFactory::createReader("Xlsx");
            $spreadsheet = $reader->load(storage_path('app/public/' . $filePath));

            if($spreadsheet->getSheetCount() < 14)
            {
                return response()->json([
                    "code"    => 414,
                    "message" => "Not Enough Working Sheets [ Expect 14 and received ".$spreadsheet->getSheetCount()." ]!"
                ], 400);
            }

            $spreadsheet->setActiveSheetIndex(0);
            $worksheet = $spreadsheet->getActiveSheet();

            if(!$this->UpdateSession($worksheet))
            {
                return response()->json([
                    "code"    => 400,
                    "message" => "Error on importing Session [$this->Error]!"
                ], 400);
            }

            $fileModel->sessionId = $this->sessionId;
            $fileModel->save();

            $spreadsheet->setActiveSheetIndex(1);
            $worksheet = $spreadsheet->getActiveSheet();

            if(!$this->UpdatePlayers($worksheet))
            {
                return response()->json([
                    "code"    => 401,
                    "message" => "Error on importing Players [$this->Error]!"
                ], 400);
            }
            
            $spreadsheet->setActiveSheetIndex(2);
            $worksheet = $spreadsheet->getActiveSheet();

            if(!$this->UpdateLongPass($worksheet))
            {
                return response()->json([
                    "code"    => 402,
                    "message" => "Error on importing Long Pass [$this->Error]!"
                ], 400);
            }
            
            $spreadsheet->setActiveSheetIndex(3);
            $worksheet = $spreadsheet->getActiveSheet();

            if(!$this->UpdateLongPassHand($worksheet))
            {
                return response()->json([
                    "code"    => 403,
                    "message" => "Error on importing Long Pass Hand [$this->Error]!"
                ], 400);
            }
            
            $spreadsheet->setActiveSheetIndex(4);
            $worksheet = $spreadsheet->getActiveSheet();

            if(!$this->UpdateTouches($worksheet))
            {
                return response()->json([
                    "code"    => 404,
                    "message" => "Error on importing Touches [$this->Error]!"
                ], 400);
            }
            
            $spreadsheet->setActiveSheetIndex(5);
            $worksheet = $spreadsheet->getActiveSheet();

            if(!$this->UpdateAgility($worksheet))
            {
                return response()->json([
                    "code"    => 405,
                    "message" => "Error on importing Agility [$this->Error]!"
                ], 400);
            }
            
            $spreadsheet->setActiveSheetIndex(6);
            $worksheet = $spreadsheet->getActiveSheet();

            if(!$this->UpdateDribble($worksheet))
            {
                return response()->json([
                    "code"    => 406,
                    "message" => "Error on importing Dribble [$this->Error]!"
                ], 400);
            }
            
            $spreadsheet->setActiveSheetIndex(7);
            $worksheet = $spreadsheet->getActiveSheet();

            if(!$this->UpdateShooting($worksheet))
            {
                return response()->json([
                    "code"    => 407,
                    "message" => "Error on importing Shooting [$this->Error]!"
                ], 400);
            }
            
            $spreadsheet->setActiveSheetIndex(8);
            $worksheet = $spreadsheet->getActiveSheet();

            if(!$this->UpdateSpeed($worksheet))
            {
                return response()->json([
                    "code"    => 408,
                    "message" => "Error on importing Speed [$this->Error]!"
                ], 400);
            }
            
            $spreadsheet->setActiveSheetIndex(9);
            $worksheet = $spreadsheet->getActiveSheet();

            if(!$this->UpdateAcceleration($worksheet))
            {
                return response()->json([
                    "code"    => 409,
                    "message" => "Error on importing Acceleration [$this->Error]!"
                ], 400);
            }
            
            $spreadsheet->setActiveSheetIndex(10);
            $worksheet = $spreadsheet->getActiveSheet();

            if(!$this->UpdateStamina($worksheet))
            {
                return response()->json([
                    "code"    => 410,
                    "message" => "Error on importing Stamina [$this->Error]!"
                ], 400);
            }
            
            $spreadsheet->setActiveSheetIndex(11);
            $worksheet = $spreadsheet->getActiveSheet();

            if(!$this->UpdateShortPass($worksheet))
            {
                return response()->json([
                    "code"    => 411,
                    "message" => "Error on importing Short Pass [$this->Error]!"
                ], 400);
            }
            
            $spreadsheet->setActiveSheetIndex(12);
            $worksheet = $spreadsheet->getActiveSheet();

            if(!$this->UpdateBiometricData($worksheet))
            {
                return response()->json([
                    "code"    => 412,
                    "message" => "Error on importing Biometric Data [$this->Error]!"
                ], 400);
            }
            
            $spreadsheet->setActiveSheetIndex(13);
            $worksheet = $spreadsheet->getActiveSheet();

            if(!$this->UpdateVerticalJump($worksheet))
            {
                return response()->json([
                    "code"    => 413,
                    "message" => "Error on importing Vertical Jump [$this->Error]!"
                ], 400);
            }

            return response()->json([
                "code"    => 200,
                "message" => "Data File Imported!"
            ], 201);
        }
        else
        {
            return response()->json([
                "code"    => 400,
                "message" => "Please select a file!"
            ], 400);
        }
   }

   function UpdateSession($worksheet)
   {
        $rowCount = 0;

        foreach ($worksheet->getRowIterator() as $row) 
        {
            if($rowCount == 0)
            {
                $rowCount++;
                continue;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
                                                                //    even if a cell value is not set.
                                                                // By default, only cells that have a value
                                                                //    set will be iterated.

            $cellCount = 0;
                
            $session = array();

            foreach ($cellIterator as $cell) 
            {
                switch($cellCount)
                {
                    case 0:
                        $session['sessionId'] = (int)$cell->getValue();
                        break;
                    case 1:
                        $session['date'] = \PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($cell->getValue());
                        $session['date'] = $session['date']->format('Y-m-d H:i:s');
                        break;
                    case 2:
                        $session['club'] = $cell->getValue();
                        break;
                    case 3:
                        $session['city'] = $cell->getValue();
                        break;
                }

                $cellCount++;
            }

            if(isset($session['date']) && !empty($session['date']))
            {
                TrainingSession::upsert($session, 'sessionId');

                $sessionData = TrainingSession::where('sessionId', '=', $session['sessionId'])->first();
                $this->sessionId = $sessionData['id'];
            }

            $rowCount++;
        }

        return true;
   }

   function UpdatePlayers($worksheet)
   {
        $rowCount = 0;

        foreach ($worksheet->getRowIterator() as $row) 
        {
            if($rowCount == 0)
            {
                $rowCount++;
                continue;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
                                                                //    even if a cell value is not set.
                                                                // By default, only cells that have a value
                                                                //    set will be iterated.

            $cellCount = 0;
                
            $player = array();

            foreach ($cellIterator as $cell) 
            {
                //var_dump($cell->getValue());

                //var_dump($cellCount);

                switch($cellCount)
                {
                    case 0:
                        $player['name'] = $cell->getValue();
                        break;
                    case 1:
                        $player['document'] = $cell->getValue();
                        break;
                    case 2:
                        $player['email'] = $cell->getValue();
                        break;
                    case 3:
                        $player['type'] = $cell->getValue();
                        break;
                    case 4:
                        $player['dorsal'] = $cell->getValue();
                        break;
                    case 5:
                        $player['age'] = $cell->getValue();
                        break;
                }

                $cellCount++;
            }

            if(isset($player['document']) && !empty($player['document']))
            {
                Players::upsert($player, 'document');

                $player = Players::where('document', '=', $player['document'])->first();

                $PlayerSession['playerId'] = $player['id'];
                $PlayerSession['sessionId'] = $this->sessionId;

                PlayersSessions::upsert($PlayerSession, ['playerId', 'sessionId']);
            }

            $rowCount++;
        }

        return true;
   }

   function UpdateLongPass($worksheet)
   {
        $rowCount = 0;

        foreach ($worksheet->getRowIterator() as $row) 
        {
            if($rowCount == 0)
            {
                $rowCount++;
                continue;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
                                                                //    even if a cell value is not set.
                                                                // By default, only cells that have a value
                                                                //    set will be iterated.

            $cellCount = 0;
            $sequence = 0;
            $data = array();

            foreach ($cellIterator as $cell) 
            {
                switch($cellCount)
                {
                    case 0:
                        break;
                    case 1:
                        if(empty($cell->getValue()))
                        {
                            $cellCount++;
                            continue 2;
                        }

                        $PlayerData =  Players::where("document", "=", $cell->getValue())->first();

                        if(!$PlayerData)
                        {
                            $this->Error = 'Player '.$cell->getValue().' Not Found!';
                            return false;
                        }

                        LongPass::where('sessionId', $this->sessionId)
                                ->where('playerId', $PlayerData["id"])->delete();

                        $data['playerId'] = $PlayerData["id"];
                        break;
                    case 2:
                        break;
                    default:
                        $data['sequence'] = $sequence;
                        $data['pass'] = $cell->getCalculatedValue();
                        $data['sessionId'] = $this->sessionId;
                        
                        if(!empty($data['pass']) || $data['pass'] === 0)
                        {
                            LongPass::upsert($data, 'sequence');
                            $sequence++;
                        }
                        break;
                }

                if($cellCount >= 13)
                {
                    break;
                }

                $cellCount++;
            }

            $rowCount++;
        }

        return true;
   }
   
   function UpdateLongPassHand($worksheet)
   {
        $rowCount = 0;

        foreach ($worksheet->getRowIterator() as $row) 
        {
            if($rowCount == 0)
            {
                $rowCount++;
                continue;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
                                                                //    even if a cell value is not set.
                                                                // By default, only cells that have a value
                                                                //    set will be iterated.

            $cellCount = 0;
            $sequence = 0;
            $data = array();

            foreach ($cellIterator as $cell) 
            {
                switch($cellCount)
                {
                    case 0:
                        break;
                    case 1:
                        if(empty($cell->getValue()))
                        {
                            $cellCount++;
                            continue 2;
                        }

                        $PlayerData =  Players::where("document", "=", $cell->getValue())->first();

                        if(!$PlayerData)
                        {
                            $this->Error = 'Player '.$cell->getValue().' Not Found!';
                            return false;
                        }

                        LongPassHand::where('sessionId', $this->sessionId)
                                    ->where('playerId', $PlayerData["id"])->delete();

                        $data['playerId'] = $PlayerData["id"];
                        break;
                    case 2:
                        break;
                    default:
                        $data['sequence'] = $sequence;
                        $data['pass'] = $cell->getCalculatedValue();
                        $data['sessionId'] = $this->sessionId;

                        if(!empty($data['pass']) || $data['pass'] === 0)
                        {
                            LongPassHand::upsert($data, 'sequence');
                            $sequence++;
                        }

                        break;
                }

                if($cellCount >= 13)
                {
                    break;
                }

                $cellCount++;
            }

            $rowCount++;
        }

        return true;
   }
   
   function UpdateTouches($worksheet)
   {        
        $rowCount = 0;

        foreach ($worksheet->getRowIterator() as $row) 
        {
            if($rowCount == 0)
            {
                $rowCount++;
                continue;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
                                                                //    even if a cell value is not set.
                                                                // By default, only cells that have a value
                                                                //    set will be iterated.

            $cellCount = 0;
            $sequence = 0;
            $data = array();

            $fieldCount = 0;

            foreach ($cellIterator as $cell) 
            {
                switch($cellCount)
                {
                    case 0:
                        break;
                    case 1:
                        if(empty($cell->getValue()))
                        {
                            $cellCount++;
                            continue 2;
                        }

                        $PlayerData =  Players::where("document", "=", $cell->getValue())->first();

                        if(!$PlayerData)
                        {
                            $this->Error = 'Player '.$cell->getValue().' Not Found!';
                            return false;
                        }

                        Touches::where('sessionId', $this->sessionId)
                               ->where('playerId', $PlayerData["id"])->delete();

                        $data['playerId'] = $PlayerData["id"];

                        break;
                    case 2:
                        break;
                    default:
                        
                        $data['sequence'] = $sequence;

                        if($fieldCount == 0)
                        {
                            $data['left'] = $cell->getCalculatedValue();
                            $fieldCount++;
                        }
                        elseif($fieldCount == 1)
                        {
                            $data['right'] = $cell->getCalculatedValue();
                            $data['sessionId'] = $this->sessionId;

                            if(!empty($data['left']) || $data['left'] === 0 ||
                               !empty($data['right']) || $data['right'] === 0)
                            {
                                Touches::upsert($data, 'sequence');
                                $sequence++;
                            }

                            $fieldCount = 0;
                        }
                        break;
                }

                if($cellCount >= 13)
                {
                    break;
                }

                $cellCount++;
            }

            $rowCount++;
        }

        return true;
   }
   
   function UpdateAgility($worksheet)
   {
        $rowCount = 0;

        foreach ($worksheet->getRowIterator() as $row) 
        {
            if($rowCount == 0)
            {
                $rowCount++;
                continue;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
                                                                //    even if a cell value is not set.
                                                                // By default, only cells that have a value
                                                                //    set will be iterated.

            $cellCount = 0;
            $sequence = 0;
            $data = array();

            foreach ($cellIterator as $cell) 
            {
                switch($cellCount)
                {
                    case 0:
                        break;
                    case 1:
                        if(empty($cell->getValue()))
                        {
                            $cellCount++;
                            continue 2;
                        }

                        $PlayerData =  Players::where("document", "=", $cell->getValue())->first();

                        if(!$PlayerData)
                        {
                            $this->Error = 'Player '.$cell->getValue().' Not Found!';
                            return false;
                        }

                        Agility::where('sessionId', $this->sessionId)
                               ->where('playerId', $PlayerData["id"])->delete();

                        $data['playerId'] = $PlayerData["id"];
                        break;
                    case 2:
                        break;
                    default:
                        $data['sequence'] = $sequence;
                        $data['try'] = $cell->getCalculatedValue();
                        $data['sessionId'] = $this->sessionId;

                        if(!empty($data['try']) || $data['try'] === 0)
                        {
                            Agility::upsert($data, 'sequence');
                        }

                        $sequence++;
                        break;
                }

                if($cellCount >= 13)
                {
                    break;
                }

                $cellCount++;
            }

            $rowCount++;
        }

        return true;
   }
   
   function UpdateDribble($worksheet)
   {
        $rowCount = 0;

        foreach ($worksheet->getRowIterator() as $row) 
        {
            if($rowCount == 0)
            {
                $rowCount++;
                continue;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
                                                                //    even if a cell value is not set.
                                                                // By default, only cells that have a value
                                                                //    set will be iterated.

            $cellCount = 0;
            $sequence = 0;
            $data = array();

            foreach ($cellIterator as $cell) 
            {
                switch($cellCount)
                {
                    case 0:
                        break;
                    case 1:
                        if(empty($cell->getValue()))
                        {
                            $cellCount++;
                            continue 2;
                        }

                        $PlayerData =  Players::where("document", "=", $cell->getValue())->first();

                        if(!$PlayerData)
                        {
                            $this->Error = 'Player '.$cell->getValue().' Not Found!';
                            return false;
                        }

                        Dribble::where('sessionId', $this->sessionId)
                               ->where('playerId', $PlayerData["id"])->delete();

                        $data['playerId'] = $PlayerData["id"];
                        break;
                    case 2:
                        break;
                    default:
                        $data['sequence'] = $sequence;
                        $data['try'] = $cell->getCalculatedValue();
                        $data['sessionId'] = $this->sessionId;

                        if(!empty($data['try']) || $data['try'] === 0)
                        {
                            Dribble::upsert($data, 'sequence');
                        }

                        $sequence++;
                        break;
                }

                if($cellCount >= 13)
                {
                    break;
                }

                $cellCount++;
            }

            $rowCount++;
        }

        return true;
   }
   
   function UpdateShooting($worksheet)
   {
        $rowCount = 0;

        foreach ($worksheet->getRowIterator() as $row) 
        {
            if($rowCount == 0)
            {
                $rowCount++;
                continue;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
                                                                //    even if a cell value is not set.
                                                                // By default, only cells that have a value
                                                                //    set will be iterated.

            $cellCount = 0;
            $sequence = 0;
            $data = array();

            $fieldCount = 0;

            foreach ($cellIterator as $cell) 
            {
                switch($cellCount)
                {
                    case 0:
                        break;
                    case 1:
                        if(empty($cell->getValue()))
                        {
                            $cellCount++;
                            continue 2;
                        }

                        $PlayerData =  Players::where("document", "=", $cell->getValue())->first();

                        if(!$PlayerData)
                        {
                            $this->Error = 'Player '.$cell->getValue().' Not Found!';
                            return false;
                        }

                        Shooting::where('sessionId', $this->sessionId)
                                ->where('playerId', $PlayerData["id"])->delete();

                        $data['playerId'] = $PlayerData["id"];

                        break;
                    case 2:
                        break;
                    default:
                        
                        $data['sequence'] = $sequence;

                        if($fieldCount == 0)
                        {
                            $data['pwr'] = $cell->getCalculatedValue();
                            $fieldCount++;
                        }
                        elseif($fieldCount == 1)
                        {
                            $data['column'] = $cell->getCalculatedValue();
                            $fieldCount++;
                        }
                        elseif($fieldCount == 2)
                        {
                            $data['line'] = $cell->getCalculatedValue();
                            $fieldCount++;
                        }
                        elseif($fieldCount == 3)
                        {
                            $data['precition'] = $cell->getCalculatedValue();
                            $fieldCount++;
                        }
                        elseif($fieldCount == 4)
                        {
                            $data['score'] = $data['pwr'] * $data['precition'];
                            $data['sessionId'] = $this->sessionId;

                            if(!empty($data['pwr']) || $data['pwr'] === 0)
                            {
                                Shooting::upsert($data, 'sequence');
                                $sequence++;
                            }

                            $fieldCount = 0;
                        }
                        break;
                }

                if($cellCount >= 17)
                {
                    break;
                }

                $cellCount++;
            }

            $rowCount++;
        }

        return true;
   }
   
   function UpdateSpeed($worksheet)
   {
        $rowCount = 0;

        foreach ($worksheet->getRowIterator() as $row) 
        {
            if($rowCount == 0)
            {
                $rowCount++;
                continue;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
                                                                //    even if a cell value is not set.
                                                                // By default, only cells that have a value
                                                                //    set will be iterated.

            $cellCount = 0;
            $sequence = 0;
            $data = array();

            foreach ($cellIterator as $cell) 
            {
                switch($cellCount)
                {
                    case 0:
                        break;
                    case 1:
                        if(empty($cell->getValue()))
                        {
                            $cellCount++;
                            continue 2;
                        }

                        $PlayerData =  Players::where("document", "=", $cell->getValue())->first();

                        if(!$PlayerData)
                        {
                            $this->Error = 'Player '.$cell->getValue().' Not Found!';
                            return false;
                        }

                        Speed::where('sessionId', $this->sessionId)
                             ->where('playerId', $PlayerData["id"])->delete();

                        $data['playerId'] = $PlayerData["id"];
                        break;
                    case 2:
                        break;
                    default:
                        $data['sequence'] = $sequence;
                        $data['time'] = $cell->getCalculatedValue();
                        $data['sessionId'] = $this->sessionId;

                        if(!empty($data['time']) || $data['time'] === 0)
                        {
                            Speed::upsert($data, 'sequence');
                        }

                        $sequence++;
                        break;
                }

                if($cellCount >= 13)
                {
                    break;
                }

                $cellCount++;
            }

            $rowCount++;
        }

        return true;
   }
   
   function UpdateAcceleration($worksheet)
   {
        $rowCount = 0;

        foreach ($worksheet->getRowIterator() as $row) 
        {
            if($rowCount == 0)
            {
                $rowCount++;
                continue;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
                                                                //    even if a cell value is not set.
                                                                // By default, only cells that have a value
                                                                //    set will be iterated.

            $cellCount = 0;
            $sequence = 0;
            $data = array();

            foreach ($cellIterator as $cell) 
            {
                switch($cellCount)
                {
                    case 0:
                        break;
                    case 1:
                        if(empty($cell->getValue()))
                        {
                            $cellCount++;
                            continue 2;
                        }

                        $PlayerData =  Players::where("document", "=", $cell->getValue())->first();

                        if(!$PlayerData)
                        {
                            $this->Error = 'Player '.$cell->getValue().' Not Found!';
                            return false;
                        }

                        Acceleration::where('sessionId', $this->sessionId)
                                    ->where('playerId', $PlayerData["id"])->delete();

                        $data['playerId'] = $PlayerData["id"];
                        break;
                    case 2:
                        break;
                    default:
                        $data['sequence'] = $sequence;
                        $data['time'] = $cell->getCalculatedValue();
                        $data['sessionId'] = $this->sessionId;

                        if(!empty($data['time']) || $data['time'] === 0)
                        {
                            Acceleration::upsert($data, 'sequence');
                        }

                        $sequence++;
                        break;
                }

                if($cellCount >= 13)
                {
                    break;
                }

                $cellCount++;
            }

            $rowCount++;
        }

        return true;
   }
   
   function UpdateStamina($worksheet)
   {
        $rowCount = 0;

        foreach ($worksheet->getRowIterator() as $row) 
        {
            if($rowCount == 0)
            {
                $rowCount++;
                continue;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
                                                                //    even if a cell value is not set.
                                                                // By default, only cells that have a value
                                                                //    set will be iterated.

            $cellCount = 0;
            $sequence = 0;
            $data = array();

            foreach ($cellIterator as $cell) 
            {
                switch($cellCount)
                {
                    case 0:
                        break;
                    case 1:
                        if(empty($cell->getValue()))
                        {
                            $cellCount++;
                            continue 2;
                        }

                        $PlayerData =  Players::where("document", "=", $cell->getValue())->first();

                        if(!$PlayerData)
                        {
                            $this->Error = 'Player '.$cell->getValue().' Not Found!';
                            return false;
                        }

                        Stamina::where('sessionId', $this->sessionId)
                               ->where('playerId', $PlayerData["id"])->delete();

                        $data['playerId'] = $PlayerData["id"];
                        break;
                    case 2:
                        break;
                    default:
                        $data['sequence'] = $sequence;
                        $data['time'] = $cell->getCalculatedValue();
                        $data['sessionId'] = $this->sessionId;

                        if(!empty($data['time']) || $data['time'] === 0)
                        {
                            Stamina::upsert($data, 'sequence');
                        }

                        $sequence++;
                        break;
                }

                if($cellCount >= 13)
                {
                    break;
                }

                $cellCount++;
            }

            $rowCount++;
        }

        return true;
   }
   
   function UpdateShortPass($worksheet)
   {
        $rowCount = 0;

        foreach ($worksheet->getRowIterator() as $row) 
        {
            if($rowCount == 0)
            {
                $rowCount++;
                continue;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
                                                                //    even if a cell value is not set.
                                                                // By default, only cells that have a value
                                                                //    set will be iterated.

            $cellCount = 0;
            $sequence = 0;
            $data = array();

            foreach ($cellIterator as $cell) 
            {
                switch($cellCount)
                {
                    case 0:
                        break;
                    case 1:
                        if(empty($cell->getValue()))
                        {
                            $cellCount++;
                            continue 2;
                        }

                        $PlayerData =  Players::where("document", "=", $cell->getValue())->first();

                        if(!$PlayerData)
                        {
                            $this->Error = 'Player '.$cell->getValue().' Not Found!';
                            return false;
                        }

                        ShortPass::where('sessionId', $this->sessionId)
                                 ->where('playerId', $PlayerData["id"])->delete();

                        $data['playerId'] = $PlayerData["id"];
                        break;
                    case 2:
                        break;
                    default:
                        $data['sequence'] = $sequence;
                        $data['try'] = $cell->getCalculatedValue();
                        $data['sessionId'] = $this->sessionId;

                        if(!empty($data['try']) || $data['try'] === 0)
                        {
                            ShortPass::upsert($data, 'sequence');
                        }

                        $sequence++;
                        break;
                }

                if($cellCount >= 13)
                {
                    break;
                }

                $cellCount++;
            }

            $rowCount++;
        }

        return true;
   }
   
   function UpdateBiometricData($worksheet)
   {
        $rowCount = 0;

        foreach ($worksheet->getRowIterator() as $row) 
        {
            if($rowCount == 0)
            {
                $rowCount++;
                continue;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
                                                                //    even if a cell value is not set.
                                                                // By default, only cells that have a value
                                                                //    set will be iterated.

            $cellCount = 0;
            $sequence = 0;

            $PlayerData = null;

            foreach ($cellIterator as $cell) 
            {
                switch($cellCount)
                {
                    case 0:
                        break;
                    case 1:
                        if(empty($cell->getValue()))
                        {
                            $cellCount++;
                            continue 2;
                        }

                        $PlayerData =  Players::where("document", "=", $cell->getValue())->first();

                        if(!$PlayerData)
                        {
                            $this->Error = 'Player '.$cell->getValue().' Not Found!';
                            return false;
                        }
                        break;
                    case 2:
                        break;
                    case 3:
                        if($PlayerData !== null)
                        {
                            $PlayerData['weight'] = $cell->getCalculatedValue();
                        }

                        break;
                    case 4:
                        
                        if($PlayerData !== null)
                        {
                            $PlayerData['height'] = $cell->getCalculatedValue();
                            $PlayerData->save();
                        }

                        $sequence++;
                        break;
                }

                if($cellCount >= 13)
                {
                    break;
                }

                $cellCount++;
            }

            $rowCount++;
        }

        return true;
   }
   
   function UpdateVerticalJump($worksheet)
   {
        $rowCount = 0;

        foreach ($worksheet->getRowIterator() as $row) 
        {
            if($rowCount == 0)
            {
                $rowCount++;
                continue;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
                                                                //    even if a cell value is not set.
                                                                // By default, only cells that have a value
                                                                //    set will be iterated.

            $cellCount = 0;
            $sequence = 0;
            $data = array();

            foreach ($cellIterator as $cell) 
            {
                switch($cellCount)
                {
                    case 0:
                        break;
                    case 1:
                        if(empty($cell->getValue()))
                        {
                            $cellCount++;
                            continue 2;
                        }

                        $PlayerData =  Players::where("document", "=", $cell->getValue())->first();

                        if(!$PlayerData)
                        {
                            $this->Error = 'Player '.$cell->getValue().' Not Found!';
                            return false;
                        }

                        VerticalJump::where('sessionId', $this->sessionId)
                                    ->where('playerId', $PlayerData["id"])->delete();

                        $data['playerId'] = $PlayerData["id"];
                        break;
                    case 2:
                        break;
                    default:
                        $data['sequence'] = $sequence;
                        $data['try'] = $cell->getCalculatedValue();
                        $data['sessionId'] = $this->sessionId;

                        if(!empty($data['try']) || $data['try'] === 0)
                        {
                            VerticalJump::upsert($data, 'sequence');
                        }

                        $sequence++;
                        break;
                }

                if($cellCount >= 13)
                {
                    break;
                }

                $cellCount++;
            }

            $rowCount++;
        }

        return true;
   }

}
