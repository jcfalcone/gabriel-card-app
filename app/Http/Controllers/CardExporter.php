<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Mail\CardMail;
use Mail;

class CardExporter extends Controller
{
    public function export(Request $req)
    {
        /*$req->validate([
            'file' => 'required|mimes:jpeg,png|max:4096'
        ]);*/

        if(!$req->file()) {
            return response()->json([
                "code"    => 400,
                "message" => "Please select a file!"
            ], 200);
        }

        if(empty($req->email))
        {
            return response()->json([
                "code"    => 401,
                "message" => "Please set a email!"
            ], 200);
        }

        $mpdf = new \Mpdf\Mpdf();
        $mpdf->imageVars['myvariable'] = file_get_contents($req->file->getRealPath());
        $mpdf->WriteHTML('<div style="position: absolute; left:0; right: 0; top: 0; bottom: 0;">
                            <img src="var:myvariable" style="width: 100%; margin: 0;" />
                          </div>');

        $disk = Storage::disk('local');

        if (!$disk->exists('pdf')) {
            $disk->makeDirectory('pdf');
        }

        $fileName = time().".pdf";
        $filePath = storage_path('app/pdf/', $fileName);

        $mpdf->Output($filePath.$fileName, "F");
        
        Mail::to($req->email)->send(new CardMail($filePath.$fileName));
        
        return response()->json([
            "code"    => 200,
            "message" => "Data Sent!"
        ], 201);
    }
}
