<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Players extends Controller
{
    public function searchPlayers(Request $req)
    {
        $query =  DB::table('players_sessions')
                            ->join('players', 'players.id', '=', 'players_sessions.playerId')
                            ->join('training_sessions', 'training_sessions.id', '=', 'players_sessions.sessionId');

        if(!empty($req->date))
        {
            $query = $query->whereBetween('training_sessions.date', [$req->date.' 00:00:00', $req->date.' 23:59:59']);
        }

        if(!empty($req->location))
        {
            $query = $query->where('training_sessions.city', 'LIKE', '%'.$req->location.'%');
        }

        if(!empty($req->sessionId) || $req->sessionId === 0)
        {
            $query = $query->where('training_sessions.sessionId', '=', $req->sessionId);
        }
        else
        {
            $query = $query->whereRaw('players_sessions.sessionId = (select max(pse.sessionId) from players_sessions pse where pse.playerId = players_sessions.playerId)');
        }

        if(!empty($req->playerId))
        {
            $query = $query->where('players.document', '=', $req->playerId);
        }

        if(!empty($req->playerEmail))
        {
            $query = $query->where('players.email', '=', $req->playerEmail);
        }

        if(!empty($req->type))
        {
            $query = $query->where('players.type', 'LIKE', '%'.$req->type.'%');
        }
                            
        $result = $query->orderBy('players.name')
                        ->get(array(
                            'players.id',
                            'players.document',
                            'players.name',
                            'training_sessions.sessionId',
                            'training_sessions.id as idSession',
                            'training_sessions.date',
                            'players.age'
                        ))->toArray();

        $Players = array();

        $PlayerModel = new \App\Models\Players();

        foreach($result as $resultData)
        {
            $Player = array();

            $Player['Identifier']   = $resultData->document;
            $Player['Session']      = $resultData->sessionId;
            $Player['SessionDate']  = date('Y-m-d', strtotime($resultData->date));
            $Player['NAME']       = $resultData->name;
            $Player['AGE']        = $resultData->age;
            $Player['AGI']        = $PlayerModel->Agility($resultData->id, $resultData->idSession, $resultData->age);
            $Player['HJ']         = $PlayerModel->HighJump($resultData->id, $resultData->idSession, $resultData->age);
            $Player['ACC']        = $PlayerModel->Acceleration($resultData->id, $resultData->idSession, $resultData->age);
            $Player['SHO']        = $PlayerModel->Shotting($resultData->id, $resultData->idSession, $resultData->age);
            $Player['DRB']        = $PlayerModel->Dribble($resultData->id, $resultData->idSession, $resultData->age);
            $Player['SPD']        = $PlayerModel->Speed($resultData->id, $resultData->idSession, $resultData->age);
            $Player['SPDKM']      = $PlayerModel->SpeedKM($resultData->id, $resultData->idSession);
            $Player['LP']         = $PlayerModel->LongPass($resultData->id, $resultData->idSession, $resultData->age);
            $Player['LPH']        = $PlayerModel->LongPassHand($resultData->id, $resultData->idSession, $resultData->age);
            $Player['STA']        = $PlayerModel->Stamina($resultData->id, $resultData->idSession, $resultData->age);
            $Player['SHP']        = $PlayerModel->ShortPass($resultData->id, $resultData->idSession, $resultData->age);

            $Players[] = $Player;
        }

        return response()->json([
            "code"    => 200,
            "message" => "",
            "players" => $Players
        ], 201);
    }

}
