<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Mail\CardMail;
use Mail;

class CreateDashboard extends Controller
{
    function GetAccuracyPosition($Column, $Row)
    {
        $Position = new \stdClass();

        $Position->X = 0;
        $Position->Y = 0;

        $RowPosition    = [0, 30, 110, 160, 210, 260];
        $ColumnPosition = [0, 40, 75, 140, 205, 270, 332, 396, 460, 523, 586, 646, 682, 717];

        $Row = (5 - $Row);

        if($Column > 0 && $Column < count($ColumnPosition))
        {
            $Position->X = $ColumnPosition[$Column];
        }

        if($Row > 0 && $Row < count($ColumnPosition))
        {
            $Position->Y = $RowPosition[$Row];
        }

        return $Position;
    }

    function GetRandomPositionInCircle($value, $circleRadius) {
        // Generate a random angle
        $randomAngle = deg2rad(mt_rand(0, 180));
        
        // Calculate the coordinates (x, y) based on the random radius and angle
        $x = $circleRadius * cos($randomAngle);
        $y = $circleRadius * sin($randomAngle);
        
        // Return the generated coordinates
        return array('x' => $x, 'y' => $y);
    }

    public function export(Request $req)
    {
        /*$req->validate([
            'file' => 'required|mimes:jpeg,png|max:4096'
        ]);*/

        if(!$req->file()) {
            return response()->json([
                "code"    => 400,
                "message" => "Please select a file!"
            ], 200);
        }

        if(!isset($req->speedGraph)) {
            return response()->json([
                "code"    => 400,
                "message" => "Please select a speed graph!"
            ], 200);
        }

        if(empty($req->email))
        {
            return response()->json([
                "code"    => 401,
                "message" => "Please set an email!"
            ], 200);
        }

        if(empty($req->sessionID))
        {
            return response()->json([
                "code"    => 402,
                "message" => "Please set a session ID!"
            ], 200);
        }

        if(empty($req->playerID))
        {
            return response()->json([
                "code"    => 403,
                "message" => "Please set a player identifier!"
            ], 200);
        }
        
        $pagePath = storage_path('app/public/dashboard/', 'Page1.png');

        $PlayerModel    = \App\Models\Players::where('document', $req->playerID)->firstOrFail();
        $Session        = \App\Models\TrainingSession::where('sessionId', $req->sessionID)->firstOrFail();

        $defaultConfig = (new \Mpdf\Config\ConfigVariables())->getDefaults();
        $fontDirs = $defaultConfig['fontDir'];

        $defaultFontConfig = (new \Mpdf\Config\FontVariables())->getDefaults();
        $fontData = $defaultFontConfig['fontdata'];

        $mpdf = new \Mpdf\Mpdf(
        [
            'fontDir' => array_merge($fontDirs, [
                $pagePath.'Fonts/Dashboard Fonts/',
                $pagePath.'Fonts/Roboto/',
            ]),
            'fontdata' => $fontData + [ // lowercase letters only in font key
                'leaguegothic' => [
                    'R' => 'LeagueGothic-Regular.ttf',
                ],
                'robotobold' => [
                    'R' => 'Roboto-Bold.ttf',
                ],
                'robotoblack' => [
                    'R' => 'Roboto-Black.ttf',
                ],
                'sourcecodeprobold' => [
                    'R' => 'SourceCodePro-Bold.ttf',
                ]
            ],
            //'default_font' => 'leaguegothic',
            'format' => [307, 477]/*,
            'debug' => true,*/
        ]);

        $mpdf->showImageErrors = true;

        $mpdf->imageVars['playerPhoto'] = file_get_contents($req->file->getRealPath());
        $mpdf->imageVars['speedGraphImg'] = file_get_contents($req->speedGraph->getRealPath());
        $mpdf->imageVars['page1'] = file_get_contents($pagePath.'Page1.png');
        $mpdf->imageVars['page2'] = file_get_contents($pagePath.'Page2.png');
        $mpdf->imageVars['page3'] = file_get_contents($pagePath.'Page3.png');
        $mpdf->imageVars['page4'] = file_get_contents($pagePath.'Page4.png');
        $mpdf->imageVars['page5'] = file_get_contents($pagePath.'Page5.png');
        $mpdf->imageVars['page6'] = file_get_contents($pagePath.'Page6.png');
        $mpdf->imageVars['page7'] = file_get_contents($pagePath.'Page7.png');
        $mpdf->imageVars['page8'] = file_get_contents($pagePath.'Page8.png');
        
        $mpdf->imageVars['ball1'] = file_get_contents($pagePath.'Elements/Ball1.png');
        $mpdf->imageVars['ball2'] = file_get_contents($pagePath.'Elements/Ball2.png');
        $mpdf->imageVars['ball3'] = file_get_contents($pagePath.'Elements/Ball3.png');
        $mpdf->imageVars['ball4'] = file_get_contents($pagePath.'Elements/Ball4.png');
        $mpdf->imageVars['ball5'] = file_get_contents($pagePath.'Elements/Ball5.png');
        $mpdf->imageVars['ball6'] = file_get_contents($pagePath.'Elements/Ball6.png');
        $mpdf->imageVars['ball7'] = file_get_contents($pagePath.'Elements/Ball7.png');
        $mpdf->imageVars['ball8'] = file_get_contents($pagePath.'Elements/Ball8.png');
        $mpdf->imageVars['ball9'] = file_get_contents($pagePath.'Elements/Ball9.png');
        $mpdf->imageVars['ball10'] = file_get_contents($pagePath.'Elements/Ball10.png');

        $Player = array();

        $Player['AgeGroup']  = strtoupper($PlayerModel->AgeGroup($PlayerModel->age));
        
        $Player['AGI']        = $PlayerModel->Agility($PlayerModel->id,   $Session->id,  $PlayerModel->age);
        $Player['HJ']         = $PlayerModel->HighJump($PlayerModel->id,  $Session->id,  $PlayerModel->age);
        $Player['ACC']        = $PlayerModel->Acceleration($PlayerModel->id,  $Session->id,  $PlayerModel->age);
        $Player['SHO']        = $PlayerModel->Shotting($PlayerModel->id,  $Session->id,  $PlayerModel->age);
        $Player['SHA']        = $PlayerModel->ShottingAccuracy($PlayerModel->id,  $Session->id,  $PlayerModel->age);
        $Player['SHP']        = $PlayerModel->ShottingPower($PlayerModel->id,  $Session->id,  $PlayerModel->age);
        $Player['DRB']        = $PlayerModel->Dribble($PlayerModel->id,   $Session->id,  $PlayerModel->age);
        $Player['SPD']        = $PlayerModel->Speed($PlayerModel->id,     $Session->id,  $PlayerModel->age);
        $Player['SPDKM']      = $PlayerModel->SpeedKM($PlayerModel->id,   $Session->id);
        $Player['ACCKM']      = $PlayerModel->AccelerationKM($PlayerModel->id,   $Session->id);
        $Player['LP']         = $PlayerModel->LongPass($PlayerModel->id,  $Session->id,  $PlayerModel->age);
        $Player['LPH']        = $PlayerModel->LongPassHand($PlayerModel->id,  $Session->id,  $PlayerModel->age);
        $Player['STA']        = $PlayerModel->Stamina($PlayerModel->id,   $Session->id,  $PlayerModel->age);
        $Player['SHP']        = $PlayerModel->ShortPass($PlayerModel->id, $Session->id,  $PlayerModel->age);
        $Player['VEJ']        = $PlayerModel->VerticalJump($PlayerModel->id, $Session->id,  $PlayerModel->age);

        if(($Player['SPD'] != 0 AND 
        $Player['LP'] != 0 AND
        $Player['SHP'] != 0 AND
        $Player['SHO'] != 0 AND
        $Player['SHA'] != 0 AND 
        $Player['SHP'] != 0 AND 
        $Player['STA'] != 0 AND 
        $Player['DRB'] != 0 AND
        $Player['AGI']))
        {
            $Player['TOTAL']      = round( ($Player['SPD'] + 
                                        $Player['LP'] + 
                                        $Player['SHP'] + 
                                        $Player['SHO'] + 
                                        $Player['SHA'] + 
                                        $Player['SHP'] + 
                                        $Player['STA'] + 
                                        $Player['DRB'] +
                                        $Player['AGI']) / 9.5, 2);
        }
        else
        {
            $Player['TOTAL']      = round( ($Player['SPD'] + 
                                        $Player['LP'] + 
                                        $Player['SHP'] + 
                                        $Player['SHO'] + 
                                        $Player['SHA'] + 
                                        $Player['SHP'] + 
                                        $Player['STA'] + 
                                        $Player['DRB'] +
                                        $Player['AGI']) / 8.55, 2);
        }

        

        $Player['STAData']    = $PlayerModel->StaminaData($PlayerModel->id, $Session->id);

        $Player['SHPTry1']    = $PlayerModel->ShortPassTry($PlayerModel->id, $Session->id, 0);
        $Player['SHPTry2']    = $PlayerModel->ShortPassTry($PlayerModel->id, $Session->id, 1);

        $Player['SHODATA']    = $PlayerModel->ShottingData($PlayerModel->id, $Session->id);

        $Player['AGITRY'] = array();
        
        $Player['AGITRY']['Try1']  = $PlayerModel->AgilityTry($PlayerModel->id,   $Session->id,  0);
        $Player['AGITRY']['Try2']  = $PlayerModel->AgilityTry($PlayerModel->id,   $Session->id,  1);

        $Player['DRBTRY'] = array();
        
        $Player['DRBTRY']['Try1']  = $PlayerModel->DribbleTry($PlayerModel->id,   $Session->id,  0);
        $Player['DRBTRY']['Try2']  = $PlayerModel->DribbleTry($PlayerModel->id,   $Session->id,  1);

        $Player['ACCTRY'] = array();

        $Player['ACCTRY']['Try1'] = $PlayerModel->AccuracyTry($PlayerModel->id, $Session->id, 0);
        $Player['ACCTRY']['Try2'] = $PlayerModel->AccuracyTry($PlayerModel->id, $Session->id, 1);
        $Player['ACCTRY']['Try3'] = $PlayerModel->AccuracyTry($PlayerModel->id, $Session->id, 2);

        $Player['SHPTry1']['PercBarWidth'] = 155 * ($Player['SHPTry1']['Perc'] / 100);
        $Player['SHPTry2']['PercBarWidth'] = 155 * ($Player['SHPTry2']['Perc'] / 100);

        if($Player['SHPTry1']['PercBarWidth'] == 0)
        {
            $Player['SHPTry1']['PercBarWidth'] = 1;
        }

        if($Player['SHPTry2']['PercBarWidth'] == 0)
        {
            $Player['SHPTry2']['PercBarWidth'] = 1;
        }

        $Player['LPHPass']['Pass'] = $PlayerModel->LongPassHandPasses($PlayerModel->id,  $Session->id);
        $Player['LPPass']['Pass']  = $PlayerModel->LongPassPasses($PlayerModel->id,  $Session->id);

        $Player['VEJData']        = $PlayerModel->VerticalJumpData($PlayerModel->id, $Session->id);

        
        switch($Player['STAData']['Battery'])
        {
            case 1:
                $mpdf->imageVars['battery1'] = file_get_contents($pagePath.'Battery/Battery1.png');
                break;
            case 2:
                $mpdf->imageVars['battery2'] = file_get_contents($pagePath.'Battery/Battery2.png');
                break;
            case 3:
                $mpdf->imageVars['battery3'] = file_get_contents($pagePath.'Battery/Battery3.png');
                break;
            case 4:
                $mpdf->imageVars['battery4'] = file_get_contents($pagePath.'Battery/Battery4.png');
                break;
            case 5:
                $mpdf->imageVars['battery5'] = file_get_contents($pagePath.'Battery/Battery5.png');
                break;
            case 6:
                $mpdf->imageVars['battery6'] = file_get_contents($pagePath.'Battery/Battery6.png');
                break;
        }

        $LongPassGrid = array();

        $LongPassGrid[] = 230;
        $LongPassGrid[] = 290;
        $LongPassGrid[] = 350;
        $LongPassGrid[] = 410;
        $LongPassGrid[] = 470;
        
        $LongPassGrid[] = 630;
        $LongPassGrid[] = 690;
        $LongPassGrid[] = 750;
        $LongPassGrid[] = 810;
        $LongPassGrid[] = 870;

        $SecondLongPassGrid[] = 780;
        $SecondLongPassGrid[] = 380;

        $LongPassMatrix = array();
        $LongPassMatrix[] = array();
        $LongPassMatrix[] = array();
        $LongPassMatrix[] = array();
        $LongPassMatrix[] = array();
        $LongPassMatrix[] = array();

        $LongPassMatrix[0]['Distance']           = 0;
        $LongPassMatrix[0]['LongPassGrid']       = $LongPassGrid;
        $LongPassMatrix[0]['SecondLongPassGrid'] = $SecondLongPassGrid;

        shuffle($LongPassMatrix[0]['LongPassGrid'] );
        shuffle($LongPassMatrix[0]['SecondLongPassGrid']);

        $LongPassMatrix[1]['Distance']           = 5;
        $LongPassMatrix[1]['LongPassGrid']       = $LongPassGrid;
        $LongPassMatrix[1]['SecondLongPassGrid'] = $SecondLongPassGrid;

        shuffle($LongPassMatrix[1]['LongPassGrid'] );
        shuffle($LongPassMatrix[1]['SecondLongPassGrid']);

        $LongPassMatrix[2]['Distance']           = 10;
        $LongPassMatrix[2]['LongPassGrid']       = $LongPassGrid;
        $LongPassMatrix[2]['SecondLongPassGrid'] = $SecondLongPassGrid;

        shuffle($LongPassMatrix[2]['LongPassGrid'] );
        shuffle($LongPassMatrix[2]['SecondLongPassGrid']);

        $LongPassMatrix[3]['Distance']           = 15;
        $LongPassMatrix[3]['LongPassGrid']       = $LongPassGrid;
        $LongPassMatrix[3]['SecondLongPassGrid'] = $SecondLongPassGrid;

        shuffle($LongPassMatrix[3]['LongPassGrid'] );
        shuffle($LongPassMatrix[3]['SecondLongPassGrid']);

        $LongPassMatrix[4]['Distance']           = 20;
        $LongPassMatrix[4]['LongPassGrid']       = $LongPassGrid;
        $LongPassMatrix[4]['SecondLongPassGrid'] = $SecondLongPassGrid;

        shuffle($LongPassMatrix[4]['LongPassGrid'] );
        shuffle($LongPassMatrix[4]['SecondLongPassGrid']);

        $LongPassMatrix[5]['Distance']           = 25;
        $LongPassMatrix[5]['LongPassGrid']       = $LongPassGrid;
        $LongPassMatrix[5]['SecondLongPassGrid'] = $SecondLongPassGrid;

        shuffle($LongPassMatrix[5]['LongPassGrid'] );
        shuffle($LongPassMatrix[5]['SecondLongPassGrid']);

        $html = '<style>
                    .defaultPage 
                    { 
                        box-decoration-break: slice;
                        position: absolute; 
                        left:0; 
                        right: 0; 
                        top: 0; 
                        bottom: 0; 
                        width:100%; 
                        height:100%; 
                        border: 1px solid black;
                    }

                    .page1
                    {
                        background-image: url(var:page1);
                        background-size: 100% 100%;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .page2
                    {
                        background-image: url(var:page2);
                        background-size: 100% 100%;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .page3
                    {
                        background-image: url(var:page3);
                        background-size: 100% 100%;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .page4
                    {
                        background-image: url(var:page4);
                        background-size: 100% 100%;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .page5
                    {
                        background-image: url(var:page5);
                        background-size: 100% 100%;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .page6
                    {
                        background-image: url(var:page6);
                        background-size: 100% 100%;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .page7
                    {
                        background-image: url(var:page7);
                        background-size: 100% 100%;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .page8
                    {
                        background-image: url(var:page8);
                        background-size: 100% 100%;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .userPhotoHolder
                    {
                        overflow:hidden;
                        border-radius: 20em;
                        width: 357px;
                        height: 357px;

                        position:absolute;
                        top: 298px;
                        left: 407px;

                        border: 0px solid yellow;
                        background-image: url(var:playerPhoto);
                        background-size: 600px 600px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .userNameHolder
                    {
                        overflow:hidden;
                        width: 900px;
                        height: 100px;

                        position:absolute;
                        top: 151px;
                        left: 130px;

                        color:white;
                        font-size: 70pt;

                        text-align: center;

                        font-family: leaguegothic;
                    }

                    .userAgeHolder
                    {
                        overflow:hidden;
                        width: 300px;
                        height: 200px;

                        position:absolute;
                        top: 335px;
                        left: 160px;

                        color:white;
                        font-size: 100pt;

                        text-align: center;

                        font-family: sourcecodeprobold;
                    }

                    .userDateHolder
                    {
                        overflow:hidden;
                        width: 300px;
                        height: 400px;

                        position:absolute;
                        top: 350px;
                        left: 730px;

                        color:white;
                        font-size: 100pt;
                        line-height: 130px;

                        text-align: center;

                        font-family: sourcecodeprobold;
                    }

                    .userAgeGroupHolder
                    {
                        overflow:hidden;
                        width: 300px;
                        height: 400px;

                        position:absolute;
                        top: 680px;
                        left: 615px;

                        color:white;
                        font-size: 38pt;

                        text-align: left;

                        font-family: leaguegothic;

                    }

                    .userFinalScoreHolder
                    {
                        overflow:hidden;
                        width: 300px;
                        height: 400px;

                        position:absolute;
                        top: 780px;
                        left: 675px;

                        color:white;
                        font-size: 60pt;

                        text-align: left;

                        font-family: leaguegothic;

                    }

                    .speedGraph
                    {
                        overflow:hidden;
                        width: 900px;
                        height: 540px;

                        position:absolute;
                        top: 1058px;
                        left: 68px;

                        background-image: url(var:speedGraphImg);
                        background-size: 955px 540px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .speedKM
                    {
                        overflow:hidden;
                        width: 250px;
                        height: 120px;

                        position:absolute;
                        top: 1208px;
                        left: 435px;

                        color:white;
                        font-size: 60pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .accelerationKM
                    {
                        overflow:hidden;
                        width: 150px;
                        height: 120px;

                        position:absolute;
                        top: 1452px;
                        left: 430px;

                        color:white;
                        font-size: 30pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .speedScore
                    {
                        overflow:hidden;
                        width: 150px;
                        height: 120px;

                        position:absolute;
                        top: 1658px;
                        left: 907px;

                        color:white;
                        font-size: 45pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .longPassScore
                    {
                        overflow:hidden;
                        width: 150px;
                        height: 120px;

                        position:absolute;
                        top: 1658px;
                        left: 907px;

                        color:white;
                        font-size: 45pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .shortPassRatio
                    {
                        overflow:hidden;
                        width: 150px;
                        height: 120px;

                        position:absolute;
                        top: 410px;
                        left: 380px;

                        color:white;
                        font-size: 50pt;
                        boder: 1px solid white;

                        font-family: robotoblack;
                    }

                    .shortPassPerc
                    {
                        overflow:hidden;
                        width: 150px;
                        height: 120px;

                        position:absolute;
                        top: 410px;
                        left: 540px;

                        color:white;
                        font-size: 50pt;
                        boder: 1px solid white;

                        font-family: robotoblack;

                        text-align:right;
                    }

                    .shortPassPercBar
                    {
                        overflow:hidden;
                        width: '.$Player['SHPTry1']['PercBarWidth'].'px;
                        height: 4px;

                        position:absolute;
                        top: 505px;
                        left: 599px;
                        
                        background-color: #2596be;
                    }

                    .shortPassTarget
                    {
                        overflow:hidden;
                        width: 150px;
                        height: 120px;

                        position:absolute;
                        top: 410px;
                        left: 895px;

                        color:white;
                        font-size: 50pt;
                        boder: 1px solid white;

                        font-family: robotoblack;
                    }

                    .shortPassRatio2
                    {
                        overflow:hidden;
                        width: 150px;
                        height: 120px;

                        position:absolute;
                        top: 570px;
                        left: 380px;

                        color:white;
                        font-size: 50pt;
                        boder: 1px solid white;

                        font-family: robotoblack;
                    }

                    .shortPassPerc2
                    {
                        overflow:hidden;
                        width: 150px;
                        height: 120px;

                        position:absolute;
                        top: 570px;
                        left: 540px;

                        color:white;
                        font-size: 50pt;
                        boder: 1px solid white;

                        font-family: robotoblack;

                        text-align:right;
                    }

                    .shortPassPercBar2
                    {
                        overflow:hidden;
                        width: '.$Player['SHPTry2']['PercBarWidth'].'px;
                        height: 4px;

                        position:absolute;
                        top: 662px;
                        left: 605px;
                        
                        background-color: #2596be;
                    }

                    .shortPassTarget2
                    {
                        overflow:hidden;
                        width: 150px;
                        height: 120px;

                        position:absolute;
                        top: 570px;
                        left: 895px;

                        color:white;
                        font-size: 50pt;
                        boder: 1px solid white;

                        font-family: robotoblack;
                    }

                    .shorpPassScore
                    {
                        overflow:hidden;
                        width: 150px;
                        height: 120px;

                        position:absolute;
                        top: 788px;
                        left: 907px;

                        color:white;
                        font-size: 45pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .accuracyPassKM1
                    {
                        overflow:hidden;
                        width: 150px;
                        height: 120px;

                        position:absolute;
                        top: 1505px;
                        left: 175px;

                        color:white;
                        font-size: 20pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .accuracyPassKM2
                    {
                        overflow:hidden;
                        width: 150px;
                        height: 120px;

                        position:absolute;
                        top: 1553px;
                        left: 175px;

                        color:white;
                        font-size: 20pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .accuracyPassKM3
                    {
                        overflow:hidden;
                        width: 150px;
                        height: 120px;

                        position:absolute;
                        top: 1602px;
                        left: 175px;

                        color:white;
                        font-size: 20pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .accuracyShotPower
                    {
                        overflow:hidden;
                        width: 150px;
                        height: 120px;

                        position:absolute;
                        top: 1503px;
                        left: 420px;

                        color:white;
                        font-size: 60pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .accuracyShotAccuracy
                    {
                        overflow:hidden;
                        width: 150px;
                        height: 120px;

                        position:absolute;
                        top: 1503px;
                        left: 620px;

                        color:white;
                        font-size: 60pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .accuracyPassScore
                    {
                        overflow:hidden;
                        width: 150px;
                        height: 120px;

                        position:absolute;
                        top: 1661px;
                        left: 907px;

                        color:white;
                        font-size: 45pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .agilityTry1
                    {
                        overflow:hidden;
                        width: 150px;
                        height: 120px;

                        position:absolute;
                        top: 379px;
                        left: 725px;

                        color:white;
                        font-size: 50pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .agilityTry2
                    {
                        overflow:hidden;
                        width: 150px;
                        height: 120px;

                        position:absolute;
                        top: 538px;
                        left: 725px;

                        color:white;
                        font-size: 50pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .agilityScore
                    {
                        overflow:hidden;
                        width: 150px;
                        height: 120px;

                        position:absolute;
                        top: 788px;
                        left: 902px;

                        color:white;
                        font-size: 45pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .dribbleTry1
                    {
                        overflow:hidden;
                        width: 150px;
                        height: 120px;

                        position:absolute;
                        top: 1233px;
                        left: 725px;

                        color:white;
                        font-size: 50pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .dribbleTry2
                    {
                        overflow:hidden;
                        width: 150px;
                        height: 120px;

                        position:absolute;
                        top: 1389px;
                        left: 725px;

                        color:white;
                        font-size: 50pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .dribbleScore
                    {
                        overflow:hidden;
                        width: 150px;
                        height: 120px;

                        position:absolute;
                        top: 1658px;
                        left: 902px;

                        color:white;
                        font-size: 45pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .longPassHandScore
                    {
                        overflow:hidden;
                        width: 150px;
                        height: 120px;

                        position:absolute;
                        top: 1661px;
                        left: 907px;

                        color:white;
                        font-size: 45pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .verticalJump
                    {
                        overflow:hidden;
                        width: 300px;
                        height: 54px;

                        position:absolute;

                        color:white;
                        font-size: 40pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .verticalJumpAttempt1
                    {
                        overflow:hidden;
                        width: 170px;
                        height: 95px;

                        position:absolute;

                        color:white;
                        font-size: 17pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .verticalJumpAttemptText1
                    {
                        font-size: 15pt;
                        color: #2596be;
                    }

                    .verticalJumpAttempt2
                    {
                        overflow:hidden;
                        width: 170px;
                        height: 95px;

                        position:absolute;

                        color:white;
                        font-size: 17pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .verticalJumpAttemptText2
                    {
                        font-size: 15pt;
                        color: #Ffeb5c;
                    }

                    .verticalJumpAttempt3
                    {
                        overflow:hidden;
                        width: 170px;
                        height: 95px;

                        position:absolute;

                        color:white;
                        font-size: 17pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .verticalJumpAttemptText3
                    {
                        font-size: 15pt;
                        color: #B3b3b3;
                    }

                    .verticalJumpScore
                    {
                        overflow:hidden;
                        width: 150px;
                        height: 120px;

                        position:absolute;
                        top: 1657px;
                        left: 907px;

                        color:white;
                        font-size: 45pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .staminaRound1
                    {
                        overflow:hidden;
                        width: 300px;
                        height: 120px;

                        position:absolute;
                        top: 285px;
                        left: 170px;

                        color:white;
                        font-size: 40pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .staminaRound2
                    {
                        overflow:hidden;
                        width: 300px;
                        height: 120px;

                        position:absolute;
                        top: 485px;
                        left: 170px;

                        color:white;
                        font-size: 40pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .staminaFatigue
                    {
                        overflow:hidden;
                        width: 300px;
                        height: 120px;

                        position:absolute;
                        top: 705px;
                        left: 170px;

                        color:white;
                        font-size: 40pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .staminaScore
                    {
                        overflow:hidden;
                        width: 150px;
                        height: 120px;

                        position:absolute;
                        top: 786px;
                        left: 907px;

                        color:white;
                        font-size: 45pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .finalScore
                    {
                        overflow:hidden;
                        width: 200px;
                        height: 140px;

                        position:absolute;
                        top: 1260px;
                        left: 685px;

                        color:white;
                        font-size: 100pt;

                        text-align: center;

                        font-family: robotoblack;
                    }

                    .ball1
                    {
                        overflow:hidden;
                        width: 58px;
                        height: 58px;

                        position:absolute;

                        background-image: url(var:ball1);
                        background-size: 58px 58px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .ball2
                    {
                        overflow:hidden;
                        width: 58px;
                        height: 58px;

                        position:absolute;

                        background-image: url(var:ball2);
                        background-size: 58px 58px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .ball3
                    {
                        overflow:hidden;
                        width: 58px;
                        height: 58px;

                        position:absolute;

                        background-image: url(var:ball3);
                        background-size: 58px 58px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .ball4
                    {
                        overflow:hidden;
                        width: 58px;
                        height: 58px;

                        position:absolute;

                        background-image: url(var:ball4);
                        background-size: 58px 58px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .ball5
                    {
                        overflow:hidden;
                        width: 58px;
                        height: 58px;

                        position:absolute;

                        background-image: url(var:ball5);
                        background-size: 58px 58px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .ball6
                    {
                        overflow:hidden;
                        width: 58px;
                        height: 58px;

                        position:absolute;

                        background-image: url(var:ball6);
                        background-size: 58px 58px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .ball7
                    {
                        overflow:hidden;
                        width: 58px;
                        height: 58px;

                        position:absolute;

                        background-image: url(var:ball7);
                        background-size: 58px 58px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .ball8
                    {
                        overflow:hidden;
                        width: 58px;
                        height: 58px;

                        position:absolute;

                        background-image: url(var:ball8);
                        background-size: 58px 58px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .ball9
                    {
                        overflow:hidden;
                        width: 58px;
                        height: 58px;

                        position:absolute;

                        background-image: url(var:ball9);
                        background-size: 58px 58px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .ball10
                    {
                        overflow:hidden;
                        width: 58px;
                        height: 58px;

                        position:absolute;

                        background-image: url(var:ball10);
                        background-size: 58px 58px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .halfBall1
                    {
                        overflow:hidden;
                        width: 40px;
                        height: 40px;

                        position:absolute;

                        background-image: url(var:ball1);
                        background-size: 40px 40px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .halfBall2
                    {
                        overflow:hidden;
                        width: 40px;
                        height: 40px;

                        position:absolute;

                        background-image: url(var:ball2);
                        background-size: 40px 40px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .halfBall3
                    {
                        overflow:hidden;
                        width: 40px;
                        height: 40px;

                        position:absolute;

                        background-image: url(var:ball3);
                        background-size: 40px 40px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .halfBall4
                    {
                        overflow:hidden;
                        width: 40px;
                        height: 40px;

                        position:absolute;

                        background-image: url(var:ball4);
                        background-size: 40px 40px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .halfBall5
                    {
                        overflow:hidden;
                        width: 40px;
                        height: 40px;

                        position:absolute;

                        background-image: url(var:ball5);
                        background-size: 40px 40px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .halfBall6
                    {
                        overflow:hidden;
                        width: 40px;
                        height: 40px;

                        position:absolute;

                        background-image: url(var:ball6);
                        background-size: 40px 40px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .halfBall7
                    {
                        overflow:hidden;
                        width: 40px;
                        height: 40px;

                        position:absolute;

                        background-image: url(var:ball7);
                        background-size: 40px 40px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .halfBall8
                    {
                        overflow:hidden;
                        width: 40px;
                        height: 40px;

                        position:absolute;

                        background-image: url(var:ball8);
                        background-size: 40px 40px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .halfBall9
                    {
                        overflow:hidden;
                        width: 40px;
                        height: 40px;

                        position:absolute;

                        background-image: url(var:ball9);
                        background-size: 40px 40px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .halfBall10
                    {
                        overflow:hidden;
                        width: 40px;
                        height: 40px;

                        position:absolute;

                        background-image: url(var:ball10);
                        background-size: 40px 40px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .battery1
                    {
                        overflow:hidden;
                        width: 431px;
                        height: 431px;

                        position:absolute;

                        background-image: url(var:battery1);
                        background-size: 431px 431px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .battery2
                    {
                        overflow:hidden;
                        width: 431px;
                        height: 431px;

                        position:absolute;

                        background-image: url(var:battery2);
                        background-size: 431px 431px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .battery3
                    {
                        overflow:hidden;
                        width: 431px;
                        height: 431px;

                        position:absolute;

                        background-image: url(var:battery3);
                        background-size: 431px 431px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .battery4
                    {
                        overflow:hidden;
                        width: 431px;
                        height: 431px;

                        position:absolute;

                        background-image: url(var:battery4);
                        background-size: 431px 431px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .battery5
                    {
                        overflow:hidden;
                        width: 431px;
                        height: 431px;

                        position:absolute;

                        background-image: url(var:battery5);
                        background-size: 431px 431px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .battery6
                    {
                        overflow:hidden;
                        width: 431px;
                        height: 431px;

                        position:absolute;

                        background-image: url(var:battery6);
                        background-size: 431px 431px;
                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }

                    .emptyIMG
                    {
                        overflow:hidden;

                        position:absolute;

                        background-repeat: no-repeat;
                        background-attachment: fixed;
                        background-position: center; 
                    }
                </style>

                <div class="defaultPage page1">
                </div>
                
                <div class="userPhotoHolder">
                </div>

                <div class="userNameHolder">
                '.mb_strtoupper($PlayerModel->name).'
                </div>

                <div class="userAgeHolder">
                '.str_pad($PlayerModel->age, 2, "0", STR_PAD_LEFT).'
                </div>

                <div class="userDateHolder">
                '.date("m", strtotime($Session->date)).'<br/>'.date("y", strtotime($Session->date)).'
                </div>

                <div class="userAgeGroupHolder">
                    '.$Player['AgeGroup'].'
                </div>

                <div class="userFinalScoreHolder">
                    '.str_pad(round($Player['TOTAL']), 2, "0", STR_PAD_LEFT).'
                </div>

                
                <div class="speedGraph">
                </div>
                
                <div class="speedScore">
                '.str_pad($Player['SPD'], 2, "0", STR_PAD_LEFT).'
                </div>
                
                <div class="speedKM">
                '.str_pad(number_format($Player['SPDKM'], 1, ','), 4, "0", STR_PAD_LEFT).'
                </div>
                
                <div class="accelerationKM">
                '.str_pad(number_format($Player['ACCKM'], 1, ','), 4, "0", STR_PAD_LEFT).'
                </div>
                
                <pagebreak />

                <div class="defaultPage page2">
                </div>';
        
        $ClosestIndex = -1;
        $ClosestDistance = 99999;

        for($count = 0; $count < count($Player['LPPass']['Pass']); $count++)
        {
            if($count >= 10)
            {
                break;
            }

            for($CountGrid = 0; $CountGrid < count($LongPassMatrix); $CountGrid++)
            {
                $Distance = $Player['LPPass']['Pass'][$count]->pass - $LongPassMatrix[$CountGrid]['Distance'];

                if($Distance < 0)
                {
                    $Distance = 9999;
                }

                if($ClosestDistance > $Distance)
                {
                    $ClosestIndex   = $CountGrid;
                    $ClosestDistance = $Distance;
                }
            }

            $PosY = 0;
            $PosX = 0;

            if($Player['LPPass']['Pass'][$count]->pass == -1)
            {
                $Positions = $this->GetRandomPositionInCircle(-1, 970 - 610);
                
                $PosY = 610 + $Positions['y'];
                $PosX = 540 + $Positions['x'];
            }
            elseif($Player['LPPass']['Pass'][$count]->pass == -2)
            {
                $Positions = $this->GetRandomPositionInCircle(-1, 920 - 610);
                
                $PosY = 610 + $Positions['y'];
                $PosX = 540 + $Positions['x'];
            }
            elseif($Player['LPPass']['Pass'][$count]->pass == -3)
            {
                $Positions = $this->GetRandomPositionInCircle(-1, 860 - 610);
                
                $PosY = 610 + $Positions['y'];
                $PosX = 540 + $Positions['x'];
            }
            elseif($Player['LPPass']['Pass'][$count]->pass == -4)
            {
                $Positions = $this->GetRandomPositionInCircle(-1, 805 - 610);
                
                $PosY = 610 + $Positions['y'];
                $PosX = 540 + $Positions['x'];
            }
            elseif($Player['LPPass']['Pass'][$count]->pass == -5)
            {
                $Positions = $this->GetRandomPositionInCircle(-1, 742 - 610);
                
                $PosY = 610 + $Positions['y'];
                $PosX = 540 + $Positions['x'];
            }
            elseif($Player['LPPass']['Pass'][$count]->pass == -6)
            {
                $Positions = $this->GetRandomPositionInCircle(-1, 735 - 610);
                
                $PosY = 610 + $Positions['y'];
                $PosX = 540 + $Positions['x'];
            }
            elseif($Player['LPPass']['Pass'][$count]->pass == -10)
            {
                $PosY = 610;
                $PosX = 540;
            }
            elseif($Player['LPPass']['Pass'][$count]->pass == -11)
            {
                $Positions = $this->GetRandomPositionInCircle(-1, 735 - 610);
                
                $PosY = 610 - $Positions['y'];
                $PosX = 540 + $Positions['x'];
            }
            else
            {
                $PosY = (380 * (1 - ($Player['LPPass']['Pass'][$count]->pass / 25))) + 1050;
                $PosX = count($LongPassMatrix[$ClosestIndex]['LongPassGrid']) > 0 ? array_pop($LongPassMatrix[$ClosestIndex]['LongPassGrid']) : 
                                                                                    array_pop($LongPassMatrix[$ClosestIndex]['SecondLongPassGrid']);
            }

            /*
            if($count%2 == 0)
            {
                $PosX = (230 * (rand(0, 100) / 100)) + 230;
            }
            else
            {
                $PosX = (230 * (rand(0, 100) / 100)) + 630;
            }*/

            $html .= '<div class="ball'.($count + 1).'" style="top: '.$PosY.'px; left: '.$PosX.'px;">
                    </div>';
        }
                
      $html .= '<div class="longPassScore">
                '.str_pad($Player['LP'], 2, "0", STR_PAD_LEFT).'
                </div>
                
                <pagebreak />

                <div class="defaultPage page3">
                </div>

                <div class="shortPassRatio">
                    '.Round($Player['SHPTry1']['Ratio'], 1).'
                </div>
                <div class="shortPassPerc">
                    '.str_pad(Round($Player['SHPTry1']['Perc']), 2, "0", STR_PAD_LEFT).'
                </div>
                <div class="shortPassPercBar">
                </div>
                <div class="shortPassTarget">
                    '.str_pad($Player['SHPTry1']['Target'], 2, "0", STR_PAD_LEFT).'
                </div>

                <div class="shortPassRatio2">
                    '.Round($Player['SHPTry2']['Ratio'], 1).'
                </div>
                <div class="shortPassPerc2">
                    '.str_pad(Round($Player['SHPTry2']['Perc']), 2, "0", STR_PAD_LEFT).'
                </div>
                <div class="shortPassPercBar2">
                </div>
                <div class="shortPassTarget2">
                    '.str_pad($Player['SHPTry2']['Target'], 2, "0", STR_PAD_LEFT).'
                </div>';
                      
        for($count = 0; $count < count($Player['SHODATA']['ShotPosition']); $count++)
        {
            if($count >= 10)
            {
                break;
            }

            $Position = $this->GetAccuracyPosition($Player['SHODATA']['ShotPosition'][$count]->column, 
                                                   $Player['SHODATA']['ShotPosition'][$count]->line);

            $html .= '<div class="halfBall'.($count + 1).'" style="top: '.(980 + $Position->Y).'px; left: '.(205 + $Position->X).'px;">
                    </div>';
        }
                
      $html .= '<div class="shorpPassScore">
                '.str_pad($Player['SHP'], 2, "0", STR_PAD_LEFT).'
                </div>

                <div class="accuracyPassKM1">
                '.Round($Player['ACCTRY']['Try1']).'
                </div>

                <div class="accuracyPassKM2">
                '.Round($Player['ACCTRY']['Try2']).'
                </div>

                <div class="accuracyPassKM3">
                '.Round($Player['ACCTRY']['Try3']).'
                </div>

                <div class="accuracyShotPower">
                '.Round($Player['SHODATA']['ShotPower']).'
                </div>

                <div class="accuracyShotAccuracy">
                '.Round($Player['SHODATA']['ShotAccuracy']).'
                </div>
                
                <div class="accuracyPassScore">
                '.str_pad($Player['ACC'], 2, "0", STR_PAD_LEFT).'
                </div>
                
                <pagebreak />

                <div class="defaultPage page4">
                </div>

                <div class="agilityTry1">
                '.$Player['AGITRY']['Try1'].'
                </div>

                <div class="agilityTry2">
                '.$Player['AGITRY']['Try2'].'
                </div>
                
                <div class="agilityScore">
                '.str_pad($Player['SHP'], 2, "0", STR_PAD_LEFT).'
                </div>

                <div class="dribbleTry1">
                '.$Player['DRBTRY']['Try1'].'
                </div>

                <div class="dribbleTry2">
                '.$Player['DRBTRY']['Try2'].'
                </div>
                
                <div class="dribbleScore">
                '.str_pad($Player['ACC'], 2, "0", STR_PAD_LEFT).'
                </div>';

        if(count($Player['LPHPass']['Pass']) > 0)
        {
            $html .= '<pagebreak />

                      <div class="defaultPage page5">
                      </div>';
                      
            for($count = 0; $count < count($Player['LPHPass']['Pass']); $count++)
            {
                if($count >= 10)
                {
                    break;
                }

                $PosY = (600 * (1 - ($Player['LPHPass']['Pass'][$count]->pass / 25))) + 530;
                $PosX = 0;

                if($count%2 == 0)
                {
                    $PosX = (350 * (rand(0, 100) / 100)) + 120;
                }
                else
                {
                    $PosX = (350 * (rand(0, 100) / 100)) + 700;
                }

                $html .= '<div class="ball'.($count + 1).'" style="top: '.$PosY.'px; left: '.$PosX.'px;">
                        </div>';
            }
                
            $html .= '<div class="longPassHandScore">
                    '.str_pad($Player['LPH'], 2, "0", STR_PAD_LEFT).'
                    </div>';
        }
                
        $html .= '<pagebreak />

                  <div class="defaultPage page6">
                  </div>';

        $TempCount = 0;
   
        for($count = count($Player['VEJData']) - 1; $count >= 0; $count--)
        {
            $PosY = ((760 * (1 - ($Player['VEJData'][$count]->try / 100))) + 535 ) - $Player['VEJData'][$count]->TextOffset;
            $PosX = 720;

            $AttemptPosY = ((760 * (1 - ($Player['VEJData'][$count]->try / 100))) + 535 ) - $Player['VEJData'][$count]->TextAttemptOffset;
            $AttemptPosX = 650;

            $BarPosX = 490;

            $BarPosY = ((760 * (1 - ($Player['VEJData'][$count]->try / 100))) + 535 ) - $Player['VEJData'][$count]->Offset;
            
            $mpdf->imageVars['Attempt'.$count] = file_get_contents($pagePath.$Player['VEJData'][$count]->Line);

            $BorderColor = "";

            $html .= '<div class="emptyIMG" style="background-image: url(var:Attempt'.$count.'); background-size: '.$Player['VEJData'][$count]->Width.'px '.$Player['VEJData'][$count]->Height.'px; width: '.$Player['VEJData'][$count]->Width.'px; height: '.$Player['VEJData'][$count]->Height.'px; top: '.$BarPosY.'px; left: '.$BarPosX.'px;">
                     </div>
                     
                     <div class="verticalJumpAttempt'.($count + 1).'" style="top: '.$AttemptPosY.'px; left: '.$AttemptPosX.'px;">
                        <div class="verticalJumpAttemptText'.($count + 1).'">'.$Player['VEJData'][$count]->Text.'</div>Attempt
                    </div>
                     
                     <div class="verticalJump" style="top: '.$PosY.'px; left: '.$PosX.'px;">
                    '.str_pad($Player['VEJData'][$count]->try, 2, "0", STR_PAD_LEFT).' cm
                    </div>';
        }

        $html .= '<div class="verticalJumpScore">
                  '.str_pad($Player['VEJ'], 2, "0", STR_PAD_LEFT).'
                  </div>
                  
                  <pagebreak />

                  <div class="defaultPage page7">
                  </div>

                  <div class="staminaRound1">
                  '.str_pad($Player['STAData']['BestTime'], 2, "0", STR_PAD_LEFT).'
                  </div>

                  <div class="staminaRound2">
                  '.str_pad($Player['STAData']['WorstTime'], 2, "0", STR_PAD_LEFT).'
                  </div>

                  <div class="staminaFatigue">
                  '.str_pad($Player['STAData']['Fatigue'], 2, "0", STR_PAD_LEFT).' %
                  </div>

                  <div class="battery'.$Player['STAData']['Battery'].'" style="top: 285px; left: 550px;">
                  </div>

                  <div class="staminaScore">
                    '.str_pad($Player['STA'], 2, "0", STR_PAD_LEFT).'
                  </div>

                  <div class="finalScore">
                    '.str_pad(round($Player['TOTAL']), 2, "0", STR_PAD_LEFT).'
                  </div>
                  
                  <pagebreak />

                  <div class="defaultPage page8">
                  </div>';

        $mpdf->WriteHTML($html);

        $disk = Storage::disk('local');

        if (!$disk->exists('pdf')) {
            $disk->makeDirectory('pdf');
        }

        $fileName = time().".pdf";
        $filePath = storage_path('app/pdf/', $fileName);

        //$mpdf->Output();

        $mpdf->Output($filePath.$fileName, "F");
        
        Mail::to($req->email)->send(new CardMail($filePath.$fileName));

        unlink($filePath.$fileName);
        
        return response()->json([
            "code"    => 200,
            "message" => "Data Sent!"
        ], 201);
    }
}
