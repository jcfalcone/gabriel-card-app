<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\File_Players;
use App\Models\ScoreData;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

class ImportScore extends Controller
{
    public function createForm()
    {
        return view('file-upload');
    }

    public function fileUpload(Request $req)
    {
        //private $sessionId = -1;
        //private $fileID = -1;
    
        /*$req->validate([
            'file' => 'required|mimes:xlsx|max:2048'
        ]);*/

        if(!$req->file()) {
            return response()->json([
                "code"    => 400,
                "message" => "Please select a file!"
            ], 200);
        }

        $fileModel = new File_Players;
        
        if($req->file()) {
            $fileName = time().'_'.$req->file->getClientOriginalName();
            $filePath = $req->file('file')->storeAs('uploads', $fileName, 'public');
            $fileModel->file = time().'_'.$req->file->getClientOriginalName();
            $fileModel->path = '/storage/' . $filePath;
            $fileModel->type = 2;
            $fileModel->save();

            $this->fileID = $fileModel->id;

            $reader = IOFactory::createReader("Xlsx");
            $spreadsheet = $reader->load(storage_path('app/public/' . $filePath));

            $spreadsheet->setActiveSheetIndex(0);
            $worksheet = $spreadsheet->getActiveSheet();

            if(!$this->UpdateScoreTable(0, $worksheet))
            {
                return response()->json([
                    "code"    => 400,
                    "message" => "Error on importing ShortPass!"
                ], 400);
            }

            if($req->part == 0)
            {
                $spreadsheet->setActiveSheetIndex(1);
                $worksheet = $spreadsheet->getActiveSheet();

                if(!$this->UpdateScoreTable(1, $worksheet))
                {
                    return response()->json([
                        "code"    => 400,
                        "message" => "Error on importing LongPass!"
                    ], 400);
                }

                $spreadsheet->setActiveSheetIndex(3);
                $worksheet = $spreadsheet->getActiveSheet();

                if(!$this->UpdateScoreTable(2, $worksheet))
                {
                    return response()->json([
                        "code"    => 400,
                        "message" => "Error on importing LongPassHand!"
                    ], 400);
                }

                $spreadsheet->setActiveSheetIndex(5);
                $worksheet = $spreadsheet->getActiveSheet();

                if(!$this->UpdateScoreTable(3, $worksheet))
                {
                    return response()->json([
                        "code"    => 400,
                        "message" => "Error on importing Acceleration!"
                    ], 400);
                }

                $spreadsheet->setActiveSheetIndex(6);
                $worksheet = $spreadsheet->getActiveSheet();

                if(!$this->UpdateScoreTable(4, $worksheet))
                {
                    return response()->json([
                        "code"    => 400,
                        "message" => "Error on importing Speed!"
                    ], 400);
                }
            }
            elseif($req->part == 1)
            {
                $spreadsheet->setActiveSheetIndex(7);
                $worksheet = $spreadsheet->getActiveSheet();

                if(!$this->UpdateScoreTable(5, $worksheet))
                {
                    return response()->json([
                        "code"    => 400,
                        "message" => "Error on importing Agility!"
                    ], 400);
                }

                $spreadsheet->setActiveSheetIndex(8);
                $worksheet = $spreadsheet->getActiveSheet();

                if(!$this->UpdateScoreTable(6, $worksheet))
                {
                    return response()->json([
                        "code"    => 400,
                        "message" => "Error on importing Dribbling!"
                    ], 400);
                }

                $spreadsheet->setActiveSheetIndex(9);
                $worksheet = $spreadsheet->getActiveSheet();

                if(!$this->UpdateScoreTable(7, $worksheet))
                {
                    return response()->json([
                        "code"    => 400,
                        "message" => "Error on importing Shooting!"
                    ], 400);
                }

                $spreadsheet->setActiveSheetIndex(10);
                $worksheet = $spreadsheet->getActiveSheet();

                if(!$this->UpdateScoreTable(8, $worksheet))
                {
                    return response()->json([
                        "code"    => 400,
                        "message" => "Error on importing Shooting Accuracy!"
                    ], 400);
                }
            }
            elseif($req->part == 2)
            {
                $spreadsheet->setActiveSheetIndex(11);
                $worksheet = $spreadsheet->getActiveSheet();

                if(!$this->UpdateScoreTable(9, $worksheet))
                {
                    return response()->json([
                        "code"    => 400,
                        "message" => "Error on importing Shooting Power!"
                    ], 400);
                }

                $spreadsheet->setActiveSheetIndex(12);
                $worksheet = $spreadsheet->getActiveSheet();

                if(!$this->UpdateScoreTable(10, $worksheet))
                {
                    return response()->json([
                        "code"    => 400,
                        "message" => "Error on importing Stamina!"
                    ], 400);
                }

                $spreadsheet->setActiveSheetIndex(13);
                $worksheet = $spreadsheet->getActiveSheet();

                if(!$this->UpdateScoreTable(11, $worksheet))
                {
                    return response()->json([
                        "code"    => 400,
                        "message" => "Error on importing Vertical Jump!"
                    ], 400);
                }

                $spreadsheet->setActiveSheetIndex(14);
                $worksheet = $spreadsheet->getActiveSheet();

                if(!$this->UpdateScoreTable(12, $worksheet))
                {
                    return response()->json([
                        "code"    => 400,
                        "message" => "Error on importing Reaction GR!"
                    ], 400);
                }
            }

            return response()->json([
                "code"    => 200,
                "message" => "Data File Imported!"
            ], 201);
        }
        else
        {
            return response()->json([
                "code"    => 400,
                "message" => "Please select a file!"
            ], 400);
        }
    }

    function CreateScoreTable($row)
    {

        $cellIterator = $row->getCellIterator();
        $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
                                                            //    even if a cell value is not set.
                                                            // By default, only cells that have a value
                                                            //    set will be iterated.

        $ScoreList = array();
        $cellCount = 0;

        foreach ($cellIterator as $cell) 
        {
            if($cellCount < 2)
            {
                $cellCount++;
                continue;
            }

            $ScoreList[] = (int)$cell->getCalculatedValue();

            $cellCount++;
        }

        return $ScoreList;
    }

    function UpdateScoreTable($Type, $worksheet)
    {
        $rowCount = 0;
        $ScoreList = array();

        ScoreData::where('type', $Type)->delete();

        foreach ($worksheet->getRowIterator() as $row) 
        {
            if($rowCount == 0)
            {
                $ScoreList = $this->CreateScoreTable($row);

                $rowCount++;
                continue;
            }

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE); // This loops through all cells,
                                                                //    even if a cell value is not set.
                                                                // By default, only cells that have a value
                                                                //    set will be iterated.

            $cellCount = 0;
                
            $session = array();

            $Age = 0;

            foreach ($cellIterator as $cell) 
            {
                switch($cellCount)
                {
                    case 0:
                        $cellCount++;
                        continue 2;
                        break;
                    case 1:  
                        $Age = $cell->getCalculatedValue();

                        if(empty($Age))
                        {
                            return true;
                        }
                        break;
                    default:
                        if(empty($cell->getCalculatedValue()) && $cell->getCalculatedValue() === NULL)
                        {
                            $cellCount++;
                            continue 2;
                        }

                        $Table = array();

                        $Table['type']   = $Type;
                        $Table['age']    = $Age;
                        $Table['gender'] = ($rowCount <= 21) ? 'M' : 'F'; 
                        $Table['result'] = $ScoreList[$cellCount - 2];
                        $Table['score']  = $cell->getCalculatedValue();

                        ScoreData::upsert($Table, ['type', 'age', 'gender', 'result']);

                        break;
                }

                $cellCount++;
            }

            $rowCount++;
        }

        return true;
    }
}