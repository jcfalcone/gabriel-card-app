<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('touches', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('playerId')->unsigned();
            $table->integer('sequence');
            $table->integer('left')->nullable();
            $table->integer('right')->nullable();
            $table->timestamps();

            $table->index('playerId');
            $table->foreign('playerId')->references('id')->on('players'); 

            $table->unique(['playerId', 'sequence']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('touches');
    }
};
