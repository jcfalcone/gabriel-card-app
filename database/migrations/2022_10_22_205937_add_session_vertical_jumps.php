<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vertical_jumps', function (Blueprint $table) {
            $table->bigInteger('sessionId')->unsigned();

            $table->index('sessionId');
            $table->foreign('sessionId')->references('id')->on('training_sessions'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vertical_jumps', function (Blueprint $table) {
            $table->dropForeign(['sessionId']);
            $table->dropColumn('sessionId');
        });
    }
};
