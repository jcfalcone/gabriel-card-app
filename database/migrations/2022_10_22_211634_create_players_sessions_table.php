<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players_sessions', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('playerId')->unsigned();
            $table->bigInteger('sessionId')->unsigned();
            $table->timestamps();

            $table->index('playerId');
            $table->foreign('playerId')->references('id')->on('players'); 

            $table->index('sessionId');
            $table->foreign('sessionId')->references('id')->on('training_sessions'); 

            $table->unique(['playerId', 'sessionId']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('players_sessions');
    }
};
