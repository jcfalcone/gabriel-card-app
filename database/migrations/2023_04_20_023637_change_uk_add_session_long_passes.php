<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('long_passes', function (Blueprint $table) {
            $table->dropUnique(['playerId', 'sequence']);
            $table->unique(['playerId', 'sequence', 'sessionId']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('long_passes', function (Blueprint $table) {
            $table->dropUnique(['playerId', 'sequence', 'sessionId']);
            $table->unique(['playerId', 'sequence']);
        });
    }
};
